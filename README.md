# 简单文档

## 准备
- 安装[io.js](https://iojs.org/en/index.html)或者node.js unstable，建议安装io.js，使用[nvm](https://github.com/creationix/nvm)管理版本
- 安装npm(包管理)和nodemon(开发环境使用nodemon，线上使用[pm2](https://github.com/Unitech/PM2))

## 开始
- git clone git@github.com:forsigner/api-seentime.git
- cd api-seentime
- npm install
- nodemon app.js(启动项目)
- 然后访问 http://127.0.0.1:3000 即可

## 技术使用
- io.js && ES6
- 后端框架使用[koa](http://koajs.com/)
- 数据库是MySql，使用了Node ORM [Sequelize](http://sequelizejs.com/)
- 使用的个各种类库请看package.json
