'use strict';

var app = require('koa')();
var debug = require('debug')('app');
var mount = require('koa-mount');
var router = require('koa-router');
var bodyParser = require('koa-body-parser');
var oauthserver = require('koa-oauth-server');
var _ = require('underscore');

// 载入路由
var uriPublic = require('./uri/public'); // 不用授权
var uriBuyer = require('./uri/buyer'); // 需用户授权
var uriSeller = require('./uri/seller'); // 需商家授权
var uriAdmin = require('./uri/admin'); // 需管理员授权

// 授权models
var oauthBuyerModel = require('./libs/oauth_buyer_model.js');
var oauthSellerModel = require('./libs/oauth_seller_model.js');

// ==================跨域=========================
app.use(function*(next) {
  this.set({
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Headers': 'Authorization, Content-Type, App-Type',
    'Access-Control-Allow-Methods': 'GET, POST, PUT, PATCH, DELETE, OPTIONS',
    'Access-Control-Expose-Headers': 'X-Pagination-Total-Count'
  });
  if (this.method == 'OPTIONS') {
    console.log(this.method);
    this.body = 'request is options';
  } else {
    yield next;
  }
});

// =oauth=================oauth=========================
// 用户授权
var oauthBuyer = oauthserver({
  model: oauthBuyerModel,
  grants: ['password', 'refresh_token'],
  accessTokenLifetime: 120960,
  debug: true
});

// 商家授权
var oauthSeller = oauthserver({
  model: oauthSellerModel,
  grants: ['password', 'refresh_token'],
  accessTokenLifetime: 120960,
  debug: true
});

// =router=================创建不同类型的路由=========================
var apiBuyerOauth = new router();
var apiSellerOauth = new router();
var apiPublic = new router();
var apiBuyer = new router();
var apiSeller = new router();
var apiAdmin = new router();

// 加载不同类型的路由
uriPublic.getRouters(apiPublic, oauthBuyer, oauthSeller); // 公共接口
uriBuyer.getRouters(apiBuyer); // 用户接口
uriSeller.getRouters(apiSeller); // 商家接口
uriAdmin.getRouters(apiAdmin); // 自营后台接口

// ==================路由限制=========================
app.use(mount('/', apiPublic.middleware()));
app.use(mount('/buyers', apiBuyer.middleware()));
app.use(mount('/sellers', apiSeller.middleware()));
app.use(mount('/admins', apiAdmin.middleware()));

app.use(mount('/buyers', oauthBuyer.authorise())); // buyer全部要验证
app.use(mount('/sellers', oauthSeller.authorise())); // seller全部要验证

// ==================错误处理=========================
app.on('error', function(err) {
  console.log(err);
});

// //==================启动服务器=========================
if (!module.parent) {
  app.listen(9000);

  console.log('listening on port 3000');

}
