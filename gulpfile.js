'use strict';

var gulp = require('gulp');
var path = require('path');
var cp = require('child_process');
var db = require('./models');
var sequelize = require('sequelize');

gulp.task('default', ['sync']);

gulp.task('sync', function() {

  var syncPromises = [
    db.Config.sync({ force: true }),
    db.Banner.sync({ force: true }),
    db.Category.sync({ force: true }),
    db.Entry.sync({ force: true }),
    db.Post.sync({ force: true }),
    db.OauthAccessToken.sync({ force: true }),
    db.OauthClient.sync({ force: true }),
    db.OauthRefreshToken.sync({ force: true }),
  ];

  var initDataPromises = [
    db.Config.create({
      invitationPoint: 100,
      invitationMoney: 100.00,
      isSwapOpen: true,
      iosPicSizeSmaller: 'smaller',
      iosPicSizeSmall: 'Small',
      iosPicSizeNormal: 'Normal',
      iosPicSizeBig: 'Big',
      iosPicSizeBigger: 'Bigger',
      aosPicSizeSmaller: 'Smaller',
      aosPicSizeSmall: 'Small',
      aosPicSizeNormal: 'Normal',
      aosPicSizeBig: 'Big',
      aosPicSizeBigger: 'Bigger'
    })
  ];

  Promise.all(syncPromises)
    .then(db.RelationBuyerTag.sync({ force: true }))
    .then(db.RelationItemTag.sync({ force: true }))
    .then(db.RelationItemCollection.sync({ force: true }))
    .then(db.RelationBuyerTopic.sync({ force: true }))
    .then(db.RelationItemTopic.sync({ force: true }))
    .then(db.BuyerBrand.sync({ force: true }))
    .then(db.BuyerItem.sync({ force: true }))
    .then(db.Seller.sync({ force: true }))
    .then(db.Brand.sync({ force: true }))
    .then(db.Seckill.sync({ force: true }))
    .then(db.Buyer.sync({ force: true }))
    .then(db.BuyerTag.sync({ force: true }))
    .then(db.ItemTag.sync({ force: true }))
    .then(db.Topic.sync({ force: true }))
    .then(db.Taobao.sync({ force: true }))
    .then(db.Task.sync({ force: true }))
    .then(function() {
      setTimeout(function() {
        db.TaskOrder.sync({ force: true });
        db.Collection.sync({ force: true });
        db.Item.sync({ force: true });
      }, 1000);

      setTimeout(function() {
        db.Message.sync({ force: true });
      }, 2000);
    })
    .then(Promise.all(initDataPromises))
    .then(function() {
      console.log('created');
    })
    .catch(function(err) {
      console.log(err);
    });

});

gulp.task('db', function() {
  var command = path.join(process.cwd(), 'node_modules', '.bin', 'sequelize') + ' db:migrate';
  cp.exec(command, {}, function(error, stdout, stderr) {
    console.log(stdout);
  });
});

gulp.task('item', function() {
  db.Item.sync({ force: true });
});

gulp.task('taobao', function() {
  db.Taobao.sync({ force: true });
});
