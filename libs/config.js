'use strict';

/**
 * 授权信息
 *
 * @property Aauth
 * @type {Object}
 */
exports.Oauth = {
  qq: {
    token_url: 'https://graph.qq.com/oauth2.0/token',
    grant_type: 'authorization_code',
    client_id: '101202182',
    client_secret: '530a22dcf51f0cc58c40df1b17eaa86e',
    redirect_uri: 'http://www.mipinr.com',
    state: 2,
    openid_url: 'https://graph.qq.com/oauth2.0/me',
    user_info_url: 'https://graph.qq.com/user/get_user_info'
  },

  jd: {
    token_url: 'https://oauth.jd.com/oauth/token',
    grant_type: 'authorization_code',
    client_id: '90093AAA62708EBD768AE161D87F2BF4',
    client_secret: '675e1f62237446de875b1f833e4b7336',
    redirect_uri: 'http://www.mipinr.com',
    state: 2
  }
};

// 默认密码
exports.defaultPassword = 'mipinr.com';
