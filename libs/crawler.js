var php = require('phpjs');
var _ = require('underscore');
_.str = require('underscore.string');
var request = require('request');
var cheerio = require('cheerio');
var gbk = require('gbk');
var moment = require('moment');
var helper = require('../libs/helper');
var url = require('url');
var querystring = require('querystring');

/**
 * 抓取淘宝账号信息
 *
 * @method getTaobaoAccountInfo
 * @param {String} name 淘宝账号
 * @param {Function} callback 回调函数
 * @return {Object} 淘宝账号信息
 */
exports.getTaobaoAccountInfo = function(name) {

  // 返回promise
  return new Promise(function(resolve, reject) {

    var url = 'http://www.taochabao.com/?a=search&nick=' + encodeURI(name);
    request(url, function(error, response, body) {

      if (!error && response.statusCode == 200) {

        // 淘宝号是否有效
        if (body.indexOf('（好评率：') < 0) {
          reject('淘宝账号无效');
        } else {
          body = body.replace(/\n/g, '');
          body = body.replace(/\r/g, '');
          body = body.replace(/\t/g, '');
          body = body.replace(/"/g, "'");

          $ = cheerio.load(body);
          var text = $('#pg1').text().replace(/\s/g, '');
          var html = $('div#box2').html();

          var type = 1;
          var isAuth;
          var goodRate;
          text.lastIndexOf('支付宝实名认证') > 0 ? isAuth = true : isAuth = false;

          // 是否有买家店
          if (text.indexOf('查看店铺最近一周') > 0) {
            goodRate = text.substring(text.indexOf('（好评率：') + 5, text.indexOf('）查看店铺最近一周'));
          } else {
            goodRate = text.substring(text.indexOf('（好评率：') + 5, text.indexOf('）最近一周'));
          }

          var signedAt = text.substring(text.indexOf('注册时间：') + 5, text.indexOf('点击这里进入淘宝'));
          var iconRank = html.substring(html.indexOf('http:'), html.indexOf('.gif') + 4);
          var allCredit = text.substring(text.indexOf('买家信用：') + 5, text.indexOf('点（好评率：'));
          var lastWeekCredit = text.substring(text.indexOf('最近一周：') + 5, text.indexOf('点最近一月'));
          var lastMonthCredit = text.substring(text.indexOf('最近一月：') + 5, text.indexOf('点最近半年'));
          var laskHalfYearCredit = text.substring(text.indexOf('最近半年：') + 5, text.indexOf('点半年以前'));
          var beforeCredit = text.substring(text.indexOf('半年以前：') + 5, text.lastIndexOf('点卖家信用'));

          // 淘宝账号信息
          var data = {
            name: name,
            type: parseInt(type),
            isAuth: isAuth,
            goodRate: goodRate,
            signedAt: moment(signedAt),
            iconRank: iconRank,
            allCredit: parseInt(allCredit),
            lastWeekCredit: parseInt(lastWeekCredit),
            lastMonthCredit: parseInt(lastMonthCredit),
            laskHalfYearCredit: parseInt(laskHalfYearCredit),
            beforeCredit: parseInt(beforeCredit)
          };

          resolve(data);
        }

      } else {
        reject(error);
      }
    });
  });
};

/**
 * 抓取html片段
 *
 * @method getHtmlFragment
 * @param {String} url
 * @return {Object} promise
 */
exports.getHtmlFragment = function(url) {

  return new Promise(function(resolve, reject) {

    request(url, function(error, response, body) {
      if (!error && response.statusCode == 200) {
        resolve(body);

      } else {
        reject(error);
      }
    });
  });
};
