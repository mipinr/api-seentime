'use strict';

/*
 * 各种自己写的中间件 by forsigner
 */
var co = require('co');
var php = require('phpjs');
var _ = require('underscore');
_.str = require('underscore.string');
var url = require('url');
var querystring = require('querystring');
var request = require('request');
var crypto = require('crypto');

var config = require('./config');

/**
 * 检查request参数
 *
 * @method checkParams
 * @param {option} option.require 必须传的参数; option.allow 允许的参数
 * @return null
 */
exports.checkParams = function(option) {

  // require, allow
  return function *(next) {

    var body = this.request.body;
    var query = this.query;
    var reqParams = _.isEmpty(body) ? query : body; // 请求传来的参数
    var reqParamsArray = _.keys(reqParams);
    var errorField = []; // 存储错误的参数

    // 必须和允许的参数
    var require = option.require || {};
    var allow = option.allow || {};

    // 检查参数是否缺少
    _.each(require, function(i) {

      // 检查所需参数有没有传过来
      if (!_.has(reqParams, i)) {
        errorField.push({ field: i, message: '缺少参数:' + i });
      }
    });

    // 是否多余
    _.each(reqParamsArray, function(i) {

      // 检查所需参数有没有传过来
      if (!_.contains(allow, i)) {
        errorField.push({ field: i, message: '参数多余:' + i });
      }
    });

    // 参数不正确
    if (!_.isEmpty(errorField)) {

      var error = {
        message: '缺少参数或者参数多余',
        errors: errorField
      };

      this.status = 422;
      this.body = error;
    } else {
      yield next;
    }
  };

};

/**
 * 检查register request参数
 *
 * @method checkRegisterParams
 * @return null
 */

exports.checkRegisterParams = function *(next) {

  var body = this.request.body || {};
  if (_.isEmpty(body) || !body.registerType) this.body = { code: 400, message: 'registerType 参数是必须的' };

  var reqParams = body; // 请求传来的参数
  var reqParamsArray = _.keys(reqParams);
  var errorField = []; // 存储错误的参数

  // 必须和允许的参数
  var require, allow;

  switch (body.registerType) {
    case '1':
      require = ['registerType', 'phone', 'password'];
      allow = require;
      break;
    case '2':
      require = ['registerType', 'nickname', 'email', 'password'];
      allow = require;
      break;
    case '3':
      require = ['registerType', 'code'];
      allow = require;
    case '4':
      require = ['registerType', 'code'];
      allow = require;
      break;
    default:
      this.body = { code: 400, message: '暂时只支持手机注册' };
      break;
  }

  // 检查参数是否缺少
  _.each(require, function(i) {

    // 检查所需参数有没有传过来
    if (!_.has(reqParams, i)) {
      errorField.push({ field: i, message: '缺少参数:' + i });
    }
  });

  // 是否多余
  _.each(reqParamsArray, function(i) {

    // 检查所需参数有没有传过来
    if (!_.contains(allow, i)) {
      errorField.push({ field: i, message: '参数多余:' + i });
    }
  });

  // 参数不正确
  if (!_.isEmpty(errorField)) {

    var error = {
      message: '缺少参数或者参数多余',
      errors: errorField
    };

    this.body = error;
  } else {
    yield next;
  }

};

/**
 * 获取token
 *
 * @method getToken
 * @params {object} ctx 上下文
 * @params {object} next
 * @return accessToken
 */
exports.getToken = function(ctx) {

  var headerToken = ctx.get('Authorization'),
      getToken =  ctx.query.accessToken,
      postToken = ctx.request.body ? ctx.request.body.accessToken : undefined;

  // Header: http://tools.ietf.org/html/rfc6750#section-2.1
  if (headerToken) {
    var matches = headerToken.match(/Bearer\s(\S+)/);
    headerToken = matches[1];
  }

  var accessToken = headerToken || postToken || getToken;

  return accessToken;
};

/**
 * 获取分页查询的条件
 *
 * @method getCondition
 * @params {object} option // 输入分页参数 如{page: 2, perPage: 20, order: '', where: {}}
 * @return {object} // 分页参数 如{page: 2, perPage: 10}
 */
exports.getCondition = function(option) {

  // 分页查询默认设置
  var config = {
    page: 1,
    perPage: 16,
    include: [],
    where: {},
    attributes: {},
    order: 'createdAt DESC'
  };

  _.extend(config, option); // 覆盖默认参数

  // 限制页码大小、限制每页数目大小
  config.page = config.page < 1 ? 1 : config.page;
  config.perPage = config.perPage < 1 ? 3 : config.perPage;
  config.perPage = config.perPage > 100 ? 100 : config.perPage;

  // 查询条件
  var condition = {
    where: config.where,
    attributes: config.attributes,
    include: config.include,
    order: config.order,
    offset: (config.page - 1) * config.perPage || 0,
    limit: config.perPage
  };

  // if(_.isEmpty(condition.where)) delete condition.where;
  if (_.isEmpty(condition.include)) delete condition.include;
  if (_.isEmpty(condition.attributes)) delete condition.attributes;

  return condition;

};

exports.getProvinces = function() {
  var provinces = ['北京', '上海', '天津', '重庆', '河北', '山西', '内蒙', '辽宁', '吉林', '黑龙江', '江苏', '浙江', '安徽', '福建', '江西', '山东', '河南', '湖北', '湖南', '广东', '广西', '海南', '四川', '贵州', '云南', '西藏', '陕西', '甘肃', '宁夏', '青海', '新疆', '香港', '澳门', '台湾', '其它'];
  return provinces;
};

/**
 * 校验itemUrl
 *
 * @method checkItemUrl
 * @param {String} url itemUrl
 * @return {boolean}
 */
exports.checkItemUrl = function(itemUrl) {

  var objUrl = url.parse(itemUrl);
  var hostname = objUrl.hostname;
  var pathname = objUrl.pathname;

  var keysQuery = _.keys(querystring.parse(objUrl.query));
  var ArrayTaobaoHostname = [
      'detail.tmall.com',
      'h5.m.taobao.com',
      'item.taobao.com',
      'detail.m.tmall.com'
  ];

  var ArrayTaobaoPathname = [
      '/item.htm',
      '/awp/core/detail.htm'
  ];

  if (!_.contains(keysQuery, 'id')) return false;
  if (!_.contains(ArrayTaobaoHostname, hostname)) return false;
  if (!_.contains(ArrayTaobaoPathname, pathname)) return false;

  return true;
};

/**
 * 获取查询历史价格索要的url
 *
 * @method getHistoryPriceUrl
 * @param {String} url itemUrl
 * @return {boolean}
 */
exports.getHistoryPriceUrl = function(itemUrl) {

  var historyPriceUrl;

  // 获取商品id
  var objUrl = url.parse(itemUrl);
  var id = querystring.parse(objUrl.query).id;

  if (itemUrl.indexOf('taobao.com') > 0) historyPriceUrl = 'http://www.xitie.com/taobao.php?no' + id;
  if (itemUrl.indexOf('tmall.com') > 0) historyPriceUrl = 'http://www.xitie.com/tmall.php?no=' + id;

  return historyPriceUrl;
};

/**
 * 生成token
 *
 * @method createToken
 * @param {String}
 * @return {boolean}
 */
exports.createToken = function(username, password) {

  return new Promise(function(resolve, reject) {

    request({
      method: 'POST',
      uri: 'http://api.seentime.com/buyer/authorize',

      // encoding : null, // 结局中文乱码问题
      form: {
        grant_type: 'password',
        client_id: '23195d43a7ab1db1f67b2c934edff6e5',
        client_secret: 'd2680c73467115d9151c8880d217b983',
        username: username,
        password: password
      }
    }, function(error, response, body) {
      if (!error) {
        resolve(JSON.parse(body));
      } else {
        reject('生成token失败');
      }
    });

  });
};

/**
 * 生成加密过的密码
 *
 * @method generatePassword
 * @param {String} password 原始秘密
 * @return {String} 加密过后的密码
 */

exports.generatePassword = function(password) {

  // 密码两次加密
  password = crypto.createHash('sha1').update(password).digest('hex');
  password = crypto.createHash('sha1').update(password).digest('hex');

  return password;
};
