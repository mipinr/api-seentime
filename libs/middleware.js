var php = require('phpjs');
var _ = require('underscore');

// ==================私有属性=========================

/**
 * sign密钥
 *
 * @property signKey
 * @type {Object}
 */
var signKey = {
  aos: 'Xc65LhOx7DH5NlvR0tgMm01TFYyLhGs5',
  ios: 'okLg9VhVTJSii12CvBYnfVCK8x1R0HZU'
};

// ==================公有属性=========================
/**
 *  config
 *
 * @property config
 * @type {Object}
 */
exports.config = {
  ios: {
    initPoints: 0, // 账户初始化积分
    inviteFartherPoints: 0, // 邀请成功师傅获得积分
    inviteSonPoints: 0, // 邀请成功徒弟获得积分
    ratio: 0.10, //分成比例
    isShow: true, //是否弹窗
    host: 'http://xiaoqianbao.github.io'
  },
  aos: {
    initPoints: 100, // 账户初始化积分
    inviteFartherPoints: 100, // 邀请成功师傅获得积分
    inviteSonPoints: 100, // 邀请成功徒弟获得积分
    ratio: 0.10, //分成比例
    isShow: true, //是否弹窗
    host: 'http://xiaoqianbao.github.io'
  }

};

/**
 * 异常处理
 *
 * @method throwError
 * @param {Object} res 响应对象
 * @param {String} type 错误码类型
 * @return null
 */
exports.throwError = function(res, type) {

  var data = {
    c: type,
    msg: error.type[type]
  };

  base.res(res, data);
};

/**
 * 检查request参数
 *
 * @method checkParams
 * @param {Object} req 请求对象
 * @param {Object} res 响应对象
 * @param {Array} require 必须传的参数
 * @param {Array} allow 允许传的参数
 * @return null
 */
exports.checkParams = function(req, res, require, allow) {

  var reqParams = _.isEmpty(req.body) ? req.query : req.body; // 请求传来的参数
  var reqParamsArray = _.keys(reqParams);
  var errorField = []; // 存储错误的参数

  /*
   *  检查参数是否缺少
   */
  _.each(require, function(i) {

    // 检查所需参数有没有传过来
    if (!_.has(reqParams, i)) {

      errorField.push(
                {
                  field: i,
                  message: '缺少参数:' + i
                }
            );
    }
  });

  _.each(reqParamsArray, function(i) {

    // 检查所需参数有没有传过来
    if (!_.contains(allow, i)) {

      errorField.push(
                {
                  field: i,
                  message: '参数多余:' + i
                }
            );
    }
  });

  /*
   * 参数不正确
   */
  if (!_.isEmpty(errorField)) {

    var error = {
      message: '缺少参数或者参数多余',
      errors: errorField
    };

    res.status(422).json(error);
  }

};
