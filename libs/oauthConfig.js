'use strict';

/**
 * 授权信息
 *
 * @property Aauth
 * @type {Object}
 */

exports.qq = {
  defaultPassword: 'mipinr.com',
  token_url: 'https://graph.qq.com/oauth2.0/token',
  grant_type: 'authorization_code',
  client_id: '101202182',
  client_secret: '530a22dcf51f0cc58c40df1b17eaa86e',
  redirect_uri: 'http://www.mipinr.com',
  state: 2,
  openid_url: 'https://graph.qq.com/oauth2.0/me',
  user_info_url: 'https://graph.qq.com/user/get_user_info'

};

exports.weibo = {
  defaultPassword: 'mipinr.com',
  token_url: 'https://api.weibo.com/oauth2/access_token',
  grant_type: 'authorization_code',
  client_id: '1857556434',
  client_secret: '2051028c812546241dd6f4452dce08ec',
  redirect_uri: 'http://www.mipinr.com',
  state: 2,
  openid_url: 'https://api.weibo.com/oauth2/get_token_info',
  user_info_url: 'https://api.weibo.com/2/users/show.json'

};

exports.jd = {
  token_url: 'https://oauth.jd.com/oauth/token',
  grant_type: 'authorization_code',
  client_id: '90093AAA62708EBD768AE161D87F2BF4',
  client_secret: '675e1f62237446de875b1f833e4b7336',
  redirect_uri: 'http://www.mipinr.com',
  state: 2
};
