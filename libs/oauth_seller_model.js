var model = module.exports;
var _ = require('underscore');
var db = require('../models');
var crypto = require('crypto');

// var bcrypt = require('bcrypt');

/*
 * 获取accessToken
 */
model.getAccessToken = function(bearerToken, callback) {
  db.OauthAccessToken
    .find({ where: { accessToken: bearerToken } })
    .then(function(data) {
      return callback(false, data);
    });
};

/*
 * 获取refreshToken
 */
model.getRefreshToken = function(bearerToken, callback) {

  db.OauthRefreshToken
    .find({ where: { refreshToken: bearerToken } })
    .then(function(data) {

      // =====================================
      // refresh bug修复，因为node-oauth2-server这个库是把client id写死为clientId的而我的是clientId，所以要转换一下
      // =====================================
      data.clientId = data.clientId;
      data.user = data.sellerId;

      return callback(false, data);
    });

};

/*
 * 获取client
 */
model.getClient = function(clientId, clientSecret, callback) {

  if (clientSecret === null) {
    db.OauthClient
      .find({ where: { clientId: clientId } })
      .then(function(data) {
        return callback(false, data);

      });
  } else {

    db.OauthClient
      .find({
        where: {
          clientId: clientId,
          clientSecret: clientSecret
        }
      })
      .then(function(data) {
        return callback(false, data);
      });
  }

};

/*
 * client是否有效
 */
model.grantTypeAllowed = function(clientId, grantType, callback) {

  if (grantType === 'password') {
    db.OauthClient
      .find({ where: { clientId: clientId } })
      .then(function(data) {

        if (!_.isEmpty(data)) {
          return callback(false, true);
        } else {
          callback(false, false);
        }
      });

  } else {
    callback(false, true);
  }

};

/*
 * 保存accessToken
 */
model.saveAccessToken = function(accessToken, clientId, expires, userId, callback) {
  db.OauthAccessToken
    .create({
      accessToken: accessToken,
      clientId: clientId,
      sellerId: userId,
      expires: expires
    }).then(function() {
    callback(false);
  });

};

/*
 * 保存refreshToken
 */
model.saveRefreshToken = function(refreshToken, clientId, expires, userId, callback) {

  db.OauthRefreshToken
    .create({
      refreshToken: refreshToken,
      clientId: clientId,
      sellerId: userId,
      expires: expires
    }).then(function() {
    callback(false);
  });

};

/*
 * 获取用户信息
 */
model.getUser = function(username, password, callback) {

  // 密码两次加密
  password = crypto.createHash('sha1').update(password).digest('hex');
  password = crypto.createHash('sha1').update(password).digest('hex');
  db.Seller
    .find({ where: { phone: username, password: password } })
    .then(function(seller) {

      if (!_.isEmpty(seller)) {
        return callback(false, seller.id);

      } else {
        callback(false, false);
      }
    });
};
