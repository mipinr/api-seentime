'use strict';

/*
 * 各种自己写的中间件 by forsigner
 */
var url = require('url');
var querystring = require('querystring');
var request = require('request');
var iconv = require('iconv-lite');
var BufferHelper = require('bufferhelper');

var oauthConfig = require('../libs/oauthConfig');

/**
 * 获取QQ授权accessToken
 *
 * @method getAccessToken
 * @param {String} code  授权码
 * @return string accessToken
 */

exports.getAccessToken = function(code) {

  return new Promise(function(resolve, reject) {

    // 请求授权接口获取accessToken数据
    request({
      method: 'GET',
      uri: oauthConfig.qq.token_url,
      encoding: null, // 中文乱码问题
      qs: {
        grant_type: oauthConfig.qq.grant_type,
        client_id: oauthConfig.qq.client_id,
        client_secret: oauthConfig.qq.client_secret,
        redirect_uri: oauthConfig.qq.redirect_uri,
        state: 2,
        code: code

      } // 查询字符串
    }, function(error, response, body) {

      if (body.indexOf('access_token=') > -1) {

        // 结局中文乱码问题
        // buf为这种字符串：access_token=YOUR_ACCESS_TOKEN&expires_in=3600
        var buf = iconv.decode(body, 'gb2312');

        // 构造一个普通url，获取其中的查询字符串
        var queryObject = querystring.parse(url.parse('http://www.something.com?' + buf).query);

        var accessToken = queryObject.access_token;

        // promise成功
        resolve(accessToken);

      } else {
        reject('获取accessToken失败');
      }
    });

  });
};

/**
 * 获取QQ授权openid
 *
 * @method getOpenid
 * @param {String} accessToken
 * @return string openid
 */
exports.getOpenid = function(accessToken) {

  return new Promise(function(resolve, reject) {

    request({
      method: 'GET',
      uri: oauthConfig.qq.openid_url,
      encoding: null, // 中文乱码问题
      qs: { access_token: accessToken } // 查询字符串
    }, function(error, response, body) {
      if (body.indexOf('openid') > -1) {

        // 结局中文乱码问题
        // buf为jsonp，格式如下
        //callback( {"client_id":"101202182","openid":"ED5661D48D300A359908BECD6563AA3C"} )
        var buf = iconv.decode(body, 'gb2312');

        // 截取jsonp里面的对象
        var jsonString = buf.substring(buf.indexOf('{') - 1, buf.indexOf('}') + 1);

        // 取得openid
        var openid = JSON.parse(jsonString).openid;

        resolve(openid);
      } else {
        reject('获取openid失败');
      }
    });

  });
};

/**
 * 获取QQ用户信息
 *
 * @method getUserInfo
 * @param {String} code  授权码
 * @return {Object} 用户信息
 */
exports.getUserInfo = function(accessToken, openid) {

  return new Promise(function(resolve, reject) {

    request({
      method: 'GET',
      uri: oauthConfig.qq.user_info_url,
      qs: {
        access_token: accessToken,
        oauth_consumer_key: oauthConfig.qq.client_id,
        openid: openid
      }
    }, function(error, response, body) {

      if (!error) {
        resolve(JSON.parse(body));
      } else {
        reject('获取用户信息失败');
      }
    });
  });
};
