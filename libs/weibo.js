'use strict';

/*
 * 各种自己写的中间件 by forsigner
 */
var url = require('url');
var querystring = require('querystring');
var request = require('request');
var iconv = require('iconv-lite');
var BufferHelper = require('bufferhelper');

var oauthConfig = require('../libs/oauthConfig');

/**
 * 获取weibo授权accessToken和uid
 *
 * @method getAccessTokenAndOpenid
 * @param {String} code  授权码
 * @return string accessToken
 */

exports.getAccessTokenAndOpenid = function(code) {

  return new Promise(function(resolve, reject) {

    // 请求授权接口获取accessToken数据
    request({
      method: 'POST',
      uri: oauthConfig.weibo.token_url,

      // encoding : null, // 中文乱码问题
      form: {
        grant_type: oauthConfig.weibo.grant_type,
        client_id: oauthConfig.weibo.client_id,
        client_secret: oauthConfig.weibo.client_secret,
        redirect_uri: oauthConfig.weibo.redirect_uri,
        state: 2,
        code: code

      } // 查询字符串
    }, function(error, response, body) {

      if (body.indexOf('access_token') > -1) {

        // promise成功
        resolve(JSON.parse(body));

      } else {
        reject('获取accessToken失败');
      }
    });

  });
};

/**
 * 获取weibo用户信息
 *
 * @method getUserInfo
 * @param {String} code  授权码
 * @return {Object} 用户信息
 */
exports.getUserInfo = function(accessToken, openid) {

  return new Promise(function(resolve, reject) {

    request({
      method: 'GET',
      uri: oauthConfig.weibo.user_info_url,
      qs: {
        access_token: accessToken,
        uid: openid
      }
    }, function(error, response, body) {

      if (!error) {
        resolve(JSON.parse(body));
      } else {
        reject('获取用户信息失败');
      }
    });
  });
};
