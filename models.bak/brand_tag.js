'use strict';

module.exports = function(sequelize, DataTypes) {

  var BrandTag = sequelize.define('BrandTag', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    name: DataTypes.STRING,
    count: DataTypes.INTEGER

  }, {
    tableName: 'brand_tag',
    timestamps: true,
    classMethods: {
      associate: function(models) {
        BrandTag.belongsToMany(models.Buyer, {
          through: models.RelationBrandTag,
          foreignKey: 'brandTagId',
          constraints: false
        });
      }
    }

  });

  return BrandTag;
};
