'use strict';

module.exports = function(sequelize, DataTypes) {

  var BuyerFinanceRecord = sequelize.define('BuyerFinanceRecord', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    sellerId: DataTypes.INTEGER,
    tradeNumber: DataTypes.STRING,
    type: DataTypes.INTEGER,
    description: DataTypes.STRING,
    balance: DataTypes.FLOAT,
    money: DataTypes.FLOAT,
    status: DataTypes.INTEGER
  }, {
    tableName: 'buyer_finance_record', // this will define the table's name
    timestamps: true, // this will deactivate the timestamp columns
    classMethods: {
      associate: function(models) {
        BuyerFinanceRecord.belongsTo(models.Seller);
      }
    }

  });

  return BuyerFinanceRecord;
};
