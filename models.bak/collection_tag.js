'use strict';

module.exports = function(sequelize, DataTypes) {

  var CollectionTag = sequelize.define('CollectionTag', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    name: DataTypes.STRING,
    count: DataTypes.INTEGER
  }, {
    tableName: 'collection_tag',
    timestamps: true,
    classMethods: {
      associate: function(models) {
        CollectionTag.belongsToMany(models.Collection, {
          through: models.RelationCollectionTag
        });
      }
    }

  });

  return CollectionTag;
};
