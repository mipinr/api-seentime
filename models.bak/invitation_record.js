'use strict';

module.exports = function(sequelize, DataTypes) {

  var InvitationRecord = sequelize.define('InvitationRecord', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    buyerId: DataTypes.INTEGER,
    sellerId: DataTypes.INTEGER,
    invitedBuyerId: DataTypes.INTEGER,
    invitedSellerId: DataTypes.INTEGER,
    point: DataTypes.INTEGER,
    money: DataTypes.FLOAT,
    status: DataTypes.INTEGER
  }, {
    tableName: 'invitation_record', // this will define the table's name
    timestamps: true, // this will deactivate the timestamp columns
    classMethods: {
      associate: function(models) {
        InvitationRecord.belongsTo(models.Buyer);
        InvitationRecord.belongsTo(models.Seller);
      }
    }

  });

  return InvitationRecord;
};
