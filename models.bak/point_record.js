'use strict';

module.exports = function(sequelize, DataTypes) {

  var PointRecord = sequelize.define('PointRecord', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    buyerId: DataTypes.INTEGER,
    tradeNumber: DataTypes.STRING,
    type: DataTypes.INTEGER,
    description: DataTypes.STRING,
    point: DataTypes.INTEGER,
    blance: DataTypes.INTEGER,
    title: DataTypes.STRING

  }, {
    tableName: 'point_record', // this will define the table's name
    timestamps: true, // this will deactivate the timestamp columns
    classMethods: {
      associate: function(models) {
        PointRecord.belongsTo(models.Buyer);
      }
    }

  });

  return PointRecord;
};
