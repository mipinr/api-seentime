'use strict';

module.exports = function(sequelize, DataTypes) {

  var RelationBuyer = sequelize.define('RelationBuyer', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    followId: DataTypes.INTEGER, // 主动关注的人
    followedId: DataTypes.INTEGER, // 被关注的人
    status: DataTypes.INTEGER
  }, {
    tableName: 'relation_buyer', // this will define the table's name
    timestamps: true, // this will deactivate the timestamp columns
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });

  return RelationBuyer;
};
