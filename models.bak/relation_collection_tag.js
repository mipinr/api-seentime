'use strict';

module.exports = function(sequelize, DataTypes) {

  var RelationCollectionTag = sequelize.define('RelationCollectionTag', {
    // uid: DataTypes.INTEGER,
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    collectionId: DataTypes.INTEGER,
    collectionTagId: DataTypes.INTEGER
  }, {
    tableName: 'relation_collection_tag', // this will define the table's name
    timestamps: true, // this will deactivate the timestamp columns
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });

  return RelationCollectionTag;
};
