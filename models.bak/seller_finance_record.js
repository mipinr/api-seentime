'use strict';

module.exports = function(sequelize, DataTypes) {

  var SellerFinanceRecord = sequelize.define('SellerFinanceRecord', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    sellerId: DataTypes.INTEGER,
    tradeNumber: DataTypes.STRING,
    type: DataTypes.INTEGER,
    description: DataTypes.STRING,
    balance: DataTypes.FLOAT,
    money: DataTypes.FLOAT,
    status: DataTypes.INTEGER
  }, {
    tableName: 'seller_finance_record', // this will define the table's name
    timestamps: true, // this will deactivate the timestamp columns
    classMethods: {
      associate: function(models) {
        SellerFinanceRecord.belongsTo(models.Seller);
      }
    }

  });

  return SellerFinanceRecord;
};
