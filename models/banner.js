'use strict';

module.exports = function(sequelize, DataTypes) {

  var Banner = sequelize.define('Banner', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    appType: DataTypes.INTEGER,
    title: DataTypes.STRING, // 标题
    picUrl: DataTypes.STRING,
    targetUrl: DataTypes.STRING,
    isShow: DataTypes.BOOLEAN
  }, {
    tableName: 'banner', // this will define the table's name
    timestamps: true, // this will deactivate the timestamp columns
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }

  });

  return Banner;
};
