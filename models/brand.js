'use strict';

module.exports = function(sequelize, DataTypes) {

  var Brand = sequelize.define('Brand', {
    // uid: DataTypes.INTEGER,
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    sourceBrandId: DataTypes.STRING,
    name: DataTypes.STRING,
    story: DataTypes.STRING,
    logoUrl: DataTypes.STRING,
    coverUrl: DataTypes.STRING,
    bannerUrl: DataTypes.STRING,
    shopUrl: DataTypes.STRING,
    detailUrl: DataTypes.STRING,
    itemListUrl: DataTypes.STRING,
    categoryId: DataTypes.INTEGER,
    firstLetter: DataTypes.STRING,
    country: DataTypes.STRING,
    followerCount: DataTypes.INTEGER,
    itemCount: DataTypes.INTEGER,
    source: DataTypes.INTEGER,
    pv: DataTypes.INTEGER,
    uv: DataTypes.INTEGER,
    status: DataTypes.INTEGER
  }, {
    tableName: 'brand', // this will define the table's name
    timestamps: true, // this will deactivate the timestamp columns
    classMethods: {
      associate: function(models) {
        Brand.hasMany(models.Item);
        Brand.belongsTo(models.Seller);
        Brand.belongsToMany(models.Buyer, {
          through: models.BuyerBrand,
          foreignKey: 'brandId',
          constraints: false
        });
      }
    }
  });

  return Brand;
};
