'use strict';

module.exports = function(sequelize, DataTypes) {

  var Buyer = sequelize.define('Buyer', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    // 用户信息
    username: DataTypes.STRING,
    password: DataTypes.STRING,
    openid: DataTypes.STRING,
    nickname: DataTypes.STRING,
    realname: DataTypes.STRING,
    intro: DataTypes.STRING,
    phone: DataTypes.STRING,
    email: DataTypes.STRING,
    gender: DataTypes.INTEGER,
    avatar: DataTypes.STRING,
    qq: DataTypes.STRING,
    alipay: DataTypes.STRING,
    birthday: DataTypes.DATE,

    city: DataTypes.STRING,
    province: DataTypes.STRING,

    followerCount: DataTypes.INTEGER,
    followingCount: DataTypes.INTEGER,

    xiaoqianIncome: DataTypes.FLOAT,
    invitationIncome: DataTypes.FLOAT,
    totalIncome: DataTypes.FLOAT,
    balanceIncome: DataTypes.FLOAT,

    taskPoint: DataTypes.FLOAT,
    signPoint: DataTypes.FLOAT,
    invitationPoint: DataTypes.FLOAT,
    totalPoint: DataTypes.FLOAT,
    balancePoint: DataTypes.FLOAT,

    registerType: DataTypes.INTEGER,

    isSigned: DataTypes.BOOLEAN,
    isInvited: DataTypes.BOOLEAN,
    lastSignTime: DataTypes.DATE,

    invitationCode: DataTypes.STRING,
    fatherInvitationCode: DataTypes.STRING

  }, {
    tableName: 'buyer', // this will define the table's name
    timestamps: true, // this will deactivate the timestamp columns
    classMethods: {
      associate: function(models) {
        Buyer.hasMany(models.TaskOrder);
        Buyer.hasMany(models.Message);
        Buyer.hasMany(models.Taobao);
        Buyer.belongsToMany(models.BuyerTag, {
          through: models.RelationBuyerTag,
          foreignKey: 'buyerId',
          constraints: false
        });
        Buyer.belongsToMany(models.Item, {
          through: models.BuyerItem,
          foreignKey: 'buyerId',
          constraints: false
        });
        Buyer.belongsToMany(models.Brand, {
          through: models.BuyerBrand,
          foreignKey: 'buyerId',
          constraints: false
        });

        Buyer.belongsToMany(models.Topic, {
          through: models.RelationBuyerTopic,
          foreignKey: 'buyerId',
          constraints: false
        });

      }
    }
  });

  return Buyer;
};
