'use strict';

module.exports = function(sequelize, DataTypes) {

  var BuyerBrand = sequelize.define('BuyerBrand', {
    // uid: DataTypes.INTEGER,
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    // buyerId: DataTypes.INTEGER,
    // brandId: DataTypes.INTEGER
  }, {
    tableName: 'buyer_brand', // this will define the table's name
    timestamps: true
  });

  return BuyerBrand;
};
