'use strict';

module.exports = function(sequelize, DataTypes) {

  var BuyerItem = sequelize.define('BuyerItem', {
    // uid: DataTypes.INTEGER,
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    buyerId: DataTypes.INTEGER,
    itemId: DataTypes.INTEGER
  }, {
    tableName: 'buyer_item', // this will define the table's name
    timestamps: true
  });

  return BuyerItem;
};
