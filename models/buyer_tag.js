'use strict';

module.exports = function(sequelize, DataTypes) {

  var BuyerTag = sequelize.define('BuyerTag', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    name: DataTypes.STRING,
    count: DataTypes.INTEGER

  }, {
    tableName: 'buyer_tag',
    timestamps: true,
    classMethods: {
      associate: function(models) {
        BuyerTag.belongsToMany(models.Buyer, {
          through: models.RelationBuyerTag,
          foreignKey: 'buyerId',
          constraints: false
        });
      }
    }
  });

  return BuyerTag;
};
