'use strict';

module.exports = function(sequelize, DataTypes) {

  var Category = sequelize.define('Category', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    name: DataTypes.STRING,
    englishName: DataTypes.STRING,
    gender: DataTypes.INTEGER,
    appType: DataTypes.INTEGER,
    isHot: DataTypes.BOOLEAN,
    weight: DataTypes.INTEGER,
    primaryCategoryId: DataTypes.INTEGER,
    primaryCategoryName: DataTypes.STRING,
    secondaryCategoryId: DataTypes.INTEGER,
    secondaryCategoryName: DataTypes.STRING,
    primaryCategoryIconClass: DataTypes.STRING,
    secondaryCategoryIconClass: DataTypes.STRING

  }, { tableName: 'category', timestamps: true });

  return Category;
};
