'use strict';

module.exports = function(sequelize, DataTypes) {

  var Collection = sequelize.define('Collection', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    // buyerId: DataTypes.INTEGER,
   // itemId: DataTypes.INTEGER,
    logoUrl: DataTypes.STRING,
    name: DataTypes.STRING,
    story: DataTypes.STRING,
    amount: DataTypes.INTEGER,
    pv: DataTypes.INTEGER,
    uv: DataTypes.INTEGER
  }, {
    tableName: 'collection', // this will define the table's name
    timestamps: true, // this will deactivate the timestamp columns
    classMethods: {
      associate: function(models) {
        Collection.belongsToMany(models.Item, {
          through: models.RelationItemCollection,
          foreignKey: 'collectionId',
          constraints: false
        });
        //Collection.belongsToMany(models.CollectionTag, {
        //  through: models.RelationCollectionTag
        //);
      }
    }
  });

  return Collection;
};
