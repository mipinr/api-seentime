module.exports = function(sequelize, Sequelize) {

  var Config = sequelize.define('Config', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    invitationPoint: Sequelize.INTEGER,
    invitationMoney: Sequelize.FLOAT,
    isSwapOpen: Sequelize.BOOLEAN,
    iosPicSizeSmaller: Sequelize.STRING,
    iosPicSizeSmall: Sequelize.STRING,
    iosPicSizeNormal: Sequelize.STRING,
    iosPicSizeBig: Sequelize.STRING,
    iosPicSizeBigger: Sequelize.STRING,
    aosPicSizeSmaller: Sequelize.STRING,
    aosPicSizeSmall: Sequelize.STRING,
    aosPicSizeNormal: Sequelize.STRING,
    aosPicSizeBig: Sequelize.STRING,
    aosPicSizeBigger: Sequelize.STRING
  }, {
    tableName: 'config', // this will define the table's name
    timestamps: true// this will deactivate the timestamp columns
 

  });

  return Config;
};
