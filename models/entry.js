'use strict';

module.exports = function(sequelize, DataTypes) {

  var Entry = sequelize.define('Entry', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    title: DataTypes.STRING, // 标题
    iosIconUrl: DataTypes.STRING,
    aosIconUrl: DataTypes.STRING,
    isHhow: DataTypes.BOOLEAN
  }, {
    tableName: 'entry', // this will define the table's name
    timestamps: true, // this will deactivate the timestamp columns
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });

  return Entry;
};
