'use strict';

var _ = require('underscore');

module.exports = function(sequelize, DataTypes) {

  var Item = sequelize.define('Item', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    // 已经定好关系，这些外键ID就不用了
    // buyerId: DataTypes.INTEGER,
    // sellerId: DataTypes.INTEGER,
    // brandId: DataTypes.INTEGER,

    // seckillId: DataTypes.INTEGER,

    title: DataTypes.STRING,
    originTitle: DataTypes.STRING,
    slogan: DataTypes.STRING,
    originPrice: DataTypes.FLOAT,
    promoPrice: DataTypes.FLOAT,
    mobilePrice: DataTypes.FLOAT,
    wechatPrice: DataTypes.FLOAT,
    discount: DataTypes.FLOAT,
    delivery: DataTypes.BOOLEAN,
    picUrl: DataTypes.STRING,
    picsUrl: {
      type: DataTypes.STRING,
      get: function() {
        var picsUrl = this.getDataValue('picsUrl');
        return _.isEmpty(picsUrl) ? picsUrl : picsUrl.split(',');
      },

      set: function(v) {
        return this.setDataValue('picsUrl', v.join(','));
      }
    },
    itemUrl: DataTypes.STRING,
    itemAlimamaUrl: DataTypes.STRING,
    promoInfo: DataTypes.STRING,
    rate: DataTypes.STRING,
    commentCount: DataTypes.INTEGER,
    selfSupport: DataTypes.INTEGER,
    saleTitle: DataTypes.STRING,
    saleInfo: DataTypes.STRING,

    saleArticle: DataTypes.TEXT,
    mobileFirst: DataTypes.BOOLEAN,
    tag: DataTypes.TEXT,

    shopTitle: DataTypes.STRING,
    shopUrl: DataTypes.STRING,
    shopLogoUrl: DataTypes.STRING,
    sellerNick: DataTypes.STRING,

    sellCount: DataTypes.INTEGER,
    favCount: DataTypes.INTEGER,
    ratio: DataTypes.FLOAT,
    goodId: DataTypes.STRING,
    categoryId: DataTypes.INTEGER,
    startTime: DataTypes.DATE,
    endTime: DataTypes.DATE,
    source: DataTypes.INTEGER,
    channel: DataTypes.INTEGER,
    appType: DataTypes.INTEGER,
    pv: DataTypes.INTEGER,
    uv: DataTypes.INTEGER,
    status: DataTypes.INTEGER

  }, {
    tableName: 'item',
    timestamps: true,
    classMethods: {
      associate: function(models) {
        // associations can be defined here

        Item.belongsTo(models.Seller);
        Item.belongsTo(models.Brand, { constraints: false });

        Item.belongsTo(models.Seckill);
        Item.belongsToMany(models.Buyer, {
          through: models.BuyerItem,
          foreignKey: 'itemId',
          constraints: false
        });
        Item.belongsToMany(models.Collection, {
          through: models.RelationItemCollection,
          foreignKey: 'itemId',
          constraints: false
        });
        Item.belongsToMany(models.Topic, {
          through: models.RelationItemTopic,
          foreignKey: 'itemId',
          constraints: false
        });
        Item.belongsToMany(models.ItemTag, {
          through: models.RelationItemTag,
          foreignKey: 'itemId',
          constraints: false
        });

      }
    }
  });

  return Item;
};
