'use strict';

module.exports = function(sequelize, DataTypes) {

  var ItemTag = sequelize.define('ItemTag', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true
    },
    name: DataTypes.STRING,
    count: DataTypes.INTEGER,
    appType: DataTypes.INTEGER

  }, {
    tableName: 'item_tag',
    timestamps: true,
    associate: function(models) {
      ItemTag.belongsToMany(models.Item, {
        through: models.RelationItemTag,
        foreignKey: 'itemTagId',
        constraints: false
      });
    }

  });

  return ItemTag;
};
