'use strict';

module.exports = function(sequelize, DataTypes) {

  var Message = sequelize.define('Message', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    // buyerId: DataTypes.INTEGER,
    // sellerId: DataTypes.INTEGER,
    // taskOrderId: DataTypes.INTEGER,
    messageItemId: DataTypes.INTEGER,
    type: DataTypes.INTEGER,
    title: DataTypes.STRING,
    picUrl: DataTypes.STRING,
    content: DataTypes.STRING,
    isReaded: DataTypes.BOOLEAN
  }, {
    tableName: 'message', // this will define the table's name
    timestamps: true, // this will deactivate the timestamp columns
    classMethods: {
      associate: function(models) {
        Message.belongsTo(models.Buyer);

        // Message.belongsTo(models.TaskOrder);
      }
    }

  });

  return Message;
};
