'use strict';

var moment = require('moment');

module.exports = function(sequelize, DataTypes) {

  var Post = sequelize.define('Post', {
    // uid: DataTypes.INTEGER,
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    title: DataTypes.STRING,
    content: DataTypes.TEXT,
    url: DataTypes.STRING,
    wroteAt: {
      type: DataTypes.DATE,
      get: function() {
        var date = this.getDataValue('wroteAt');
        return moment(date).format('YYYY-MM-DD');
      }
    }
  }, {
    tableName: 'post', // this will define the table's name
    timestamps: true // this will deactivate the timestamp columns
  });

  return Post;
};
