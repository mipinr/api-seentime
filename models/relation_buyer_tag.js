'use strict';

module.exports = function(sequelize, DataTypes) {

  var RelationBuyerTag = sequelize.define('RelationBuyerTag', {
    // uid: DataTypes.INTEGER,
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    itemId: DataTypes.INTEGER,
    buyerTagId: DataTypes.INTEGER
  }, {
    tableName: 'relation_buyer_tag', // this will define the table's name
    timestamps: true, // this will deactivate the timestamp columns
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });

  return RelationBuyerTag;
};
