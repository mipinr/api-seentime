'use strict';

module.exports = function(sequelize, DataTypes) {

  var RelationBuyerTopic = sequelize.define('RelationBuyerTopic', {
    // uid: DataTypes.INTEGER,
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    buyerId: DataTypes.INTEGER,
    topicId: DataTypes.INTEGER
  }, {
    tableName: 'relation_buyer_topic', // this will define the table's name
    timestamps: true // this will deactivate the timestamp columns

  });

  return RelationBuyerTopic;
};
