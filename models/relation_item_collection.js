'use strict';

module.exports = function(sequelize, DataTypes) {

  var RelationItemCollection = sequelize.define('RelationItemCollection', {
    // uid: DataTypes.INTEGER,
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    itemId: DataTypes.INTEGER,
    collectionId: DataTypes.INTEGER
  }, {
    tableName: 'relation_item_collection', // this will define the table's name
    timestamps: true // this will deactivate the timestamp columns
  });

  return RelationItemCollection;
};
