'use strict';

module.exports = function(sequelize, DataTypes) {

  var RelationItemTag = sequelize.define('RelationItemTag', {
    // uid: DataTypes.INTEGER,
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    itemId: DataTypes.INTEGER,
    tagItemId: DataTypes.INTEGER
  }, {
    tableName: 'relation_item_tag', // this will define the table's name
    timestamps: true // this will deactivate the timestamp columns
  });

  return RelationItemTag;
};
