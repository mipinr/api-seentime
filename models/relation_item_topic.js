'use strict';

module.exports = function(sequelize, DataTypes) {

  var RelationItemTopic = sequelize.define('RelationItemTopic', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    itemId: DataTypes.INTEGER,
    topicId: DataTypes.INTEGER
  },
    {
      tableName: 'relation_item_topic', // this will define the table's name
      timestamps: true // this will deactivate the timestamp columns
    });

  return RelationItemTopic;
};
