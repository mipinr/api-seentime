'use strict';

module.exports = function(sequelize, DataTypes) {

  var Seckill = sequelize.define('Seckill', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    sourceBrandId: DataTypes.STRING,
    name: DataTypes.STRING,
    slogan: DataTypes.STRING,
    smallImgUrl: DataTypes.STRING,
    detailUrl: DataTypes.STRING,
    bigImgUrl: DataTypes.STRING,
    logoUrl: DataTypes.STRING,
    leaveTime: DataTypes.INTEGER,
    discount: DataTypes.INTEGER,
    promoInfo: DataTypes.STRING,
    source: DataTypes.INTEGER,
    pv: DataTypes.INTEGER,
    uv: DataTypes.INTEGER
  }, {
    tableName: 'seckill', // this will define the table's name
    timestamps: true, // this will deactivate the timestamp columns
    classMethods: {
      associate: function(models) {
        Seckill.belongsTo(models.Seller);
        Seckill.hasMany(models.Item);
      }
    }
  });

  return Seckill;
};
