'use strict';

module.exports = function(sequelize, DataTypes) {

  var Seller = sequelize.define('Seller', {
    // uid: DataTypes.INTEGER,
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    // 用户信息
    username: DataTypes.STRING,
    password: DataTypes.STRING,
    avatar: DataTypes.STRING,
    qq: DataTypes.STRING,
    phone: DataTypes.STRING,
    email: DataTypes.STRING,
    alipay: DataTypes.STRING,
    wangwang: DataTypes.STRING,
    shopName: DataTypes.STRING,
    storeUrl: DataTypes.STRING,
    hongbaoExpense: DataTypes.FLOAT,
    shiyongExpense: DataTypes.FLOAT,
    xiaoqianExpense: DataTypes.FLOAT,
    invitationIncome: DataTypes.FLOAT,
    balance: DataTypes.FLOAT,
    rank: DataTypes.INTEGER,
    isInvited: DataTypes.BOOLEAN,
    invitationCode: DataTypes.STRING,
    fatherInvitationCode: DataTypes.STRING

  }, {
    tableName: 'seller', // this will define the table's name
    timestamps: true, // this will deactivate the timestamp columns
    classMethods: {
      associate: function(models) {
        Seller.hasMany(models.Task);
        Seller.hasMany(models.TaskOrder);
        Seller.hasMany(models.Item);
        Seller.hasMany(models.Brand);

        Seller.hasMany(models.Topic);
        Seller.hasMany(models.Seckill);
      }
    }
  });

  return Seller;
};
