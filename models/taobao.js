'use strict';

module.exports = function(sequelize, DataTypes) {

  var Taobao = sequelize.define('Taobao', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    name: DataTypes.STRING, // 用户名
    type: DataTypes.INTEGER,
    isAuth: DataTypes.BOOLEAN,
    signedAt: DataTypes.DATE,
    goodRate: DataTypes.STRING,
    iconRank: DataTypes.STRING,
    allCredit: DataTypes.INTEGER,
    lastWeekCredit: DataTypes.INTEGER,
    lastMonthCredit: DataTypes.INTEGER,
    lastHalfYearCredit: DataTypes.INTEGER,
    beforeCredit: DataTypes.INTEGER,
    ip: DataTypes.STRING,
    region: DataTypes.STRING,
    ipType: DataTypes.STRING,
    isDeleted: DataTypes.BOOLEAN

  }, {
    tableName: 'taobao', // this will define the table's name
    timestamps: true, // this will deactivate the timestamp columns
    classMethods: {
      associate: function(models) {
        Taobao.belongsTo(models.Buyer, { constraints: false });
        Taobao.hasMany(models.TaskOrder);
      }
    }

  });

  return Taobao;
};
