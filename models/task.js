'use strict';

module.exports = function(sequelize, DataTypes) {

  var Task = sequelize.define('Task', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    taskType: DataTypes.INTEGER,
    deviceType: DataTypes.INTEGER,
    orderType: DataTypes.INTEGER,
    auditType: DataTypes.INTEGER,

    itemUrl: DataTypes.STRING,
    itemBUrl: DataTypes.STRING,
    source: DataTypes.INTEGER,
    delivery: DataTypes.BOOLEAN,
    title: DataTypes.STRING, // 标题
    originTitle: DataTypes.STRING,
    slogan: DataTypes.STRING,
    originPrice: DataTypes.FLOAT,
    promoPrice: DataTypes.FLOAT,
    picUrl: DataTypes.STRING,
    sellCount: DataTypes.INTEGER,
    province: DataTypes.STRING,
    city: DataTypes.STRING,
    shopTitle: DataTypes.STRING,
    shopurl: DataTypes.STRING,
    shopLogoUrl: DataTypes.STRING,
    sellerNick: DataTypes.STRING,

    amount: DataTypes.INTEGER,
    reward: DataTypes.FLOAT,
    guarantyMoney: DataTypes.FLOAT,
    remain: DataTypes.INTEGER,
    applyCount: DataTypes.INTEGER,
    passCount: DataTypes.INTEGER,
    finishCount: DataTypes.INTEGER,
    secret: DataTypes.STRING,
    startTime: DataTypes.DATE,
    endTime: DataTypes.DATE,
    keyword: DataTypes.STRING,
    ctrlFWord: DataTypes.STRING,
    searchType: DataTypes.INTEGER,
    searchSort: DataTypes.INTEGER,
    position: DataTypes.STRING,
    minTime: DataTypes.DATE,
    maxTime: DataTypes.DATE,
    searchPicUrl: DataTypes.STRING,
    basicRequirement: DataTypes.STRING,
    otherRequirement: DataTypes.STRING,

    question: DataTypes.STRING,
    answer: DataTypes.STRING,
    screenshot: DataTypes.STRING,

    signedAt: DataTypes.DATE,
    isAuth: DataTypes.BOOLEAN,
    iconRank: DataTypes.STRING,
    lastWeekCredit: DataTypes.INTEGER,
    lastMonthCredit: DataTypes.INTEGER,

    status: DataTypes.INTEGER
  }, {
    tableName: 'task', // this will define the table's name
    timestamps: true, // this will deactivate the timestamp columns
    classMethods: {
      associate: function(models) {
        Task.belongsTo(models.Seller);
        Task.hasMany(models.TaskOrder);
      }
    }

  });

  return Task;
};
