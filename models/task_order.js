'use strict';

module.exports = function(sequelize, DataTypes) {

  var TaskOrder = sequelize.define('TaskOrder', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    orderNumber: DataTypes.STRING,
    status: DataTypes.INTEGER
  }, {
    tableName: 'task_order', // this will define the table's name
    timestamps: true, // this will deactivate the timestamp columns
    classMethods: {
      associate: function(models) {
        TaskOrder.belongsTo(models.Buyer);
        TaskOrder.belongsTo(models.Seller);
        TaskOrder.belongsTo(models.Task);
        TaskOrder.belongsTo(models.Taobao);

        // TaskOrder.hasMany(models.Message);
      }
    }

  });

  return TaskOrder;
};
