'use strict';

module.exports = function(sequelize, DataTypes) {

  var Topic = sequelize.define('Topic', {
    // uid: DataTypes.INTEGER,
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    name: DataTypes.STRING,
    story: DataTypes.STRING,
    logoUrl: DataTypes.STRING,
    coverUrl: DataTypes.STRING,
    categoryId: DataTypes.INTEGER,
    followerCount: DataTypes.INTEGER,
    itemCount: DataTypes.INTEGER,
    pv: DataTypes.INTEGER,
    uv: DataTypes.INTEGER
  }, {
    tableName: 'topic', // this will define the table's name
    timestamps: true, // this will deactivate the timestamp columns
    classMethods: {
      associate: function(models) {
        Topic.belongsTo(models.Seller);
        Topic.belongsToMany(models.Buyer, {
          through: models.RelationBuyerTopic,
          foreignKey: 'topicId',
          constraints: false
        });
        Topic.belongsToMany(models.Item, {
          through: models.RelationItemTopic,
          foreignKey: 'topicId',
          constraints: false
        });
      }
    }
  });

  return Topic;
};
