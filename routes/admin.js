var db = require('../models')
var helper = require('../libs/helper');
var crawler = require('../libs/crawler');
var _ = require('underscore');
_.str = require('underscore.string');


exports.getAdmin = function * () {
    // this.body = 'created_at DESC';
};

exports.updateAdmin = function * () {
};

exports.getItems = function * () {
   var query = this.query;

    // 获取分页查询条件
    var condition = helper.getCondition({
        where: {sellerId: this.params.sellerId },
        page: query.page,
        perPage: query.perPage
    });

    if(query.channel) {
        condition.where.channel = query.channel;
    }

    var data = yield db.Item.findAndCountAll(condition);
    this.set({'X-Pagination-Total-Count': data.count});
    this.body = data.rows;

};

// 创建商品
exports.createItem = function * () {

    var body = this.request.body || {};
    body.sellerId = this.params.sellerId;

    // 创建商品
    var newItem = yield db.Item.create(body);
    newItem.id = yield db.Item.max('id');
    this.body = newItem;
};


// 修改商品
exports.updateItem = function * () {
    var body = this.request.body || {};
    yield db.Item.update(body, {where:{id: this.params.itemId}});
    this.body = yield db.Item.find({where:{id: this.params.itemId}});
};

