var co = require('co');
var swig  = require('swig');
var views = require('co-views');

var render = views(__dirname + '/../views', {
  map: {
    html: 'swig'
  }
});

// api说明
exports.index = function *() {

    var mipinrs = [

        {
            name: '配置信息',
            url: 'http://api.seentime.com/config',
            type: 'GET',
            status: 1,
            detailUrl: 'https://tower.im/projects/a8a8dc71074f492bb4975c879f3fe8cf/docs/78c380c0d82b415cae890fd88c0f11c1/'
        },
        {
            name: 'banner列表',
            url: 'http://api.seentime.com/banners',
            type: 'GET',
            status: 1,
            detailUrl: 'https://tower.im/projects/a8a8dc71074f492bb4975c879f3fe8cf/docs/2fb0ab87e25c4d7685c1b9dbe6acc844/'
        },
        {
            name: '类目',
            url: 'http://api.seentime.com/categorys',
            type: 'GET',
            status: 1,
            detailUrl: 'https://tower.im/projects/a8a8dc71074f492bb4975c879f3fe8cf/docs/b7c0f598930944f297550abbb7dc17ac/'
        },
        {
            name: '商品列表',
            url: 'http://api.seentime.com/items',
            type: 'GET',
            status: 1,
            detailUrl: 'https://tower.im/projects/a8a8dc71074f492bb4975c879f3fe8cf/docs/ce8e4daf279f4022808c50aa177ad530/'
        },
        {
            name: '单个商品',
            url: 'http://api.seentime.com/items/:itemId',
            type: 'GET',
            status: 1,
            detailUrl: 'https://tower.im/projects/a8a8dc71074f492bb4975c879f3fe8cf/docs/b15354f33bef4a1bbb9aac528f2561ab/'
        },
        {
            name: '品牌列表',
            url: 'http://api.seentime.com/brands',
            type: 'GET',
            status: 1,
            detailUrl: 'https://tower.im/projects/a8a8dc71074f492bb4975c879f3fe8cf/docs/92fd98536bdf41a5b9f9333acfa437d3/'
        },
        {
            name: '单个品牌',
            url: 'http://api.seentime.com/brands/:brandId',
            type: 'GET',
            status: 1,
            detailUrl: 'https://tower.im/projects/a8a8dc71074f492bb4975c879f3fe8cf/docs/cbdf5a7db8e046da848db20ecfc61002/'
        },
        {
            name: '专题列表',
            url: 'http://api.seentime.com/topics',
            type: 'GET',
            status: 1,
            detailUrl: 'https://tower.im/projects/a8a8dc71074f492bb4975c879f3fe8cf/docs/d66b41a54ad54a8397faebf4d1618c15/'
        },
        {
            name: '单个专题',
            url: 'http://api.seentime.com/topics/:topicId',
            type: 'GET',
            status: 1,
            detailUrl: 'https://tower.im/projects/a8a8dc71074f492bb4975c879f3fe8cf/docs/c6a3262f7dd14537a54d5900aefcbfdf/'
        },

        {
            name: '闪购列表',
            url: 'http://api.seentime.com/seckills',
            type: 'GET',
            status: 1,
            detailUrl: 'https://tower.im/projects/a8a8dc71074f492bb4975c879f3fe8cf/docs/b3dbf327cc6c4d36b0e6467c21b8a4c5/'
        },
        {
            name: '单个闪购',
            url: 'http://api.seentime.com/seckills/:seckillId',
            type: 'GET',
            status: 1,
            detailUrl: 'https://tower.im/projects/a8a8dc71074f492bb4975c879f3fe8cf/docs/dd4459aba6f54ea6bb59d7184e28d59d/'
        },
        {
            name: '搜索',
            url: 'http://api.seentime.com/search',
            type: 'GET',
            status: 1,
            detailUrl: 'https://tower.im/projects/a8a8dc71074f492bb4975c879f3fe8cf/docs/55c38ebe5d1e4d6996ebccac6cb31e73/'
        },

    ];


    //=====================================================================

    var publics = [

        {
            name: '配置信息',
            url: 'http://api.seentime.com/config',
            type: 'GET',
            status: 1,
            detailUrl: 'https://tower.im/projects/f36d2393669943619e47cdded84d1ffa/docs/479f295c0f1245f2951a12acae824479/'
        },
        {
            name: 'banner列表',
            url: 'http://api.seentime.com/banners',
            type: 'GET',
            status: 1,
            detailUrl: 'https://tower.im/projects/f36d2393669943619e47cdded84d1ffa/docs/c0a4b6fd51734f42b8bea5247e45a63a/'
        },
        {
            name: 'app首页入口列表',
            url: 'http://api.seentime.com/entrys',
            type: 'GET',
            status: 1,
            detailUrl: 'https://tower.im/projects/f36d2393669943619e47cdded84d1ffa/docs/3484a46ff0354855b5c7ca8a6591eeb8'
        },
        {
            name: '获取商品列表',
            url: 'http://api.seentime.com/items',
            type: 'GET',
            status: 1,
            detailUrl: 'https://tower.im/projects/f36d2393669943619e47cdded84d1ffa/docs/67c1de61553a40bc891ed1997ca4be6d/'
        },
        {
            name: '获取单个商品',
            url: 'http://api.seentime.com/items/:itemId',
            type: 'GET',
            status: 1,
            detailUrl: 'https://tower.im/projects/f36d2393669943619e47cdded84d1ffa/docs/95f624d7bc574c7b9b8808d0038e3dd3/'
        },
        {
            name: '创建商品',
            url: 'http://api.seentime.com/items/:itemId',
            type: 'POST',
            status: 0,
            detailUrl: 'https://tower.im/projects/f36d2393669943619e47cdded84d1ffa/docs/5d81c63b8a504dc0bcf9b4829478d333/'
        },
        {
            name: '更新商品',
            url: 'http://api.seentime.com/items/:itemId',
            type: 'PATCH',
            status: 0,
            detailUrl: 'https://tower.im/projects/f36d2393669943619e47cdded84d1ffa/docs/8ab75f41b49645aab0fccc2d62332632/'
        },
        {
            name: '任务列表',
            url: 'http://api.seentime.com/tasks',
            type: 'GET',
            status: 1,
            detailUrl: 'https://tower.im/projects/f36d2393669943619e47cdded84d1ffa/docs/81ab72b8ce4046118c361f93a1b09add/'
        },
        {
            name: '单个任务',
            url: 'http://api.seentime.com/hongbaos/:taskId',
            type: 'GET',
            status: 1,
            detailUrl: 'https://tower.im/projects/f36d2393669943619e47cdded84d1ffa/docs/9ce95a22297d4b0ca6516bd8251cfbb7/'
        },
        {
            name: '该任务的用户申请情况',
            url: 'http://api.seentime.com/tasks/:taskId/task_orders',
            type: 'GET',
            status: 1,
            detailUrl: 'https://tower.im/projects/f36d2393669943619e47cdded84d1ffa/docs/5ef71a820cc6489a8b076513433787ba/'
        },
        {
            name: '抓取淘宝账号信息',
            url: 'http://api.seentime.com/taobao_account_info',
            type: 'GET',
            status: 1,
            detailUrl: 'https://tower.im/projects/f36d2393669943619e47cdded84d1ffa/docs/29e7f625743f4388916345cbd1dccdf6/'
        },
        {
            name: '抓取商品信息',
            url: 'http://api.seentime.com/item_info',
            type: 'post',
            status: 1,
            detailUrl: 'https://tower.im/projects/f36d2393669943619e47cdded84d1ffa/docs/c404842323f14a10b62633c7792eed58/'
        },
        {
            name: '获取类目',
            url: 'http://api.seentime.com/categorys',
            type: 'get',
            status: 1,
            detailUrl: 'https://tower.im/projects/f36d2393669943619e47cdded84d1ffa/docs/29410565cfa54ca29355a33637276df5/'
        },
        {
            name: '获取达人榜单',
            url: 'http://api.seentime.com/top_lists',
            type: 'GET',
            status: 1,
            detailUrl: 'https://tower.im/projects/f36d2393669943619e47cdded84d1ffa/docs/56b9e3a6396e4fda987b8645de56b704/'
        }

    ];


    var buyers = [
        {
            name: '用户注册',
            url: 'http://api.seentime.com/buyer/register',
            type: 'POST',
            status: 1,
            detailUrl: 'https://tower.im/projects/f36d2393669943619e47cdded84d1ffa/docs/5f2ec13a6ef041348684c369408797e4/'
        },
        {
            name: '用户找回密码',
            url: 'http://api.seentime.com/buyer/password_reset',
            type: 'PATCH',
            status: 1,
            detailUrl: 'https://tower.im/projects/f36d2393669943619e47cdded84d1ffa/docs/1df8039a15c14ebb9ff1d789e5afd88d/'
        },
        {
            name: '用户登录(通过用户名和密码获取token)',
            url: 'http://api.seentime.com/buyer/authorize',
            type: 'POST',
            status: 1,
            detailUrl: 'https://tower.im/projects/f36d2393669943619e47cdded84d1ffa/docs/5e907ad6c05b4f3b8f0e2eb287f5ffef/'
        },
        {
            name: '用户登录(通过refreshToken获取token)',
            url: 'http://api.seentime.com/buyer/authorize',
            type: 'POST',
            status: 1,
            detailUrl: 'https://tower.im/projects/f36d2393669943619e47cdded84d1ffa/docs/076c6cbd8394484f9a2e2dcfb22227a5/'
        },
        {
            name: '获取用户信息(使用token)',
            url: 'http://api.seentime.com/buyers/info',
            type: 'GET',
            status: 1,
            detailUrl: 'https://tower.im/projects/f36d2393669943619e47cdded84d1ffa/docs/9a75dec2208a4d08ab520ce96749253d/'
        },
        {
            name: '获取用户信息',
            url: 'http://api.seentime.com/buyers/:buyerId',
            type: 'GET',
            status: 1,
            detailUrl: 'https://tower.im/projects/f36d2393669943619e47cdded84d1ffa/docs/479f295c0f1245f2951a12acae824479/'
        },
        {
            name: '更新用户信息',
            url: 'http://api.seentime.com/buyers/:buyerId',
            type: 'PATCH',
            status: 1,
            detailUrl: 'https://tower.im/projects/f36d2393669943619e47cdded84d1ffa/docs/c34e98a5f4694e8ea73eef08b7388643/'
        },
        {
            name: '用户申请任务资格',
            url: 'http://api.seentime.com/buyers/:buyerId/task_orders',
            type: 'POST',
            status: 1,
            detailUrl: 'https://tower.im/projects/f36d2393669943619e47cdded84d1ffa/docs/16abf12bcb9e41728ebab4e74153eb51/'
        },
        {
            name: '某个用户的任务订单列表',
            url: 'http://api.seentime.com/buyers/:buyerId/task_orders',
            type: 'GET',
            status: 1,
            detailUrl: 'https://tower.im/projects/f36d2393669943619e47cdded84d1ffa/docs/bda426a78b2a408d811cb009cfffbb71/'
        },
        {
            name: '用户更新单个任务订单(提交订单号、放弃资格等)',
            url: 'http://api.seentime.com/buyers/:buyerId/hongbao_orders/:hongbaoOrderId',
            type: 'PATCH',
            status: 1,
            detailUrl: 'https://tower.im/projects/f36d2393669943619e47cdded84d1ffa/docs/b87fb12483fc41b29ebcfd0e0bbe6a2e/'
        },
        {
            name: '用户单个任务订单详情',
            url: 'http://api.seentime.com/buyers/:buyerId/task_orders/:taskOrderId',
            type: 'GET',
            status: 0,
            detailUrl: 'https://tower.im/projects/f36d2393669943619e47cdded84d1ffa/docs/64a9e740533e45388125f63f780708d7/'
        },
        {
            name: '用户添加一个淘宝小号',
            url: 'http://api.seentime.com/buyers/:buyerId/taobaos',
            type: 'POST',
            status: 1,
            detailUrl: 'https://tower.im/projects/f36d2393669943619e47cdded84d1ffa/docs/434f629daa8c445a85d2dfcb1457a7f5/'
        },
        {
            name: '用户淘宝小号列表',
            url: 'http://api.seentime.com/buyers/:buyerId/taobaos',
            type: 'GET',
            status: 1,
            detailUrl: 'https://tower.im/projects/f36d2393669943619e47cdded84d1ffa/docs/e86707060d604ba7846fe812866402ec/'
        },
        {
            name: '更新一个淘宝小号',
            url: 'http://api.seentime.com/buyers/:buyerId/taobaos/taobao_id',
            type: 'PATCH',
            status: 1,
            detailUrl: 'https://tower.im/projects/f36d2393669943619e47cdded84d1ffa/docs/c7dd1fab3fff40e48b2fe4c1cb69eca5/'
        },
        {
            name: '删除一个淘宝小号',
            url: 'http://api.seentime.com/buyers/:buyerId/taobaos/taobao_id',
            type: 'DELETE',
            status: 1,
            detailUrl: 'https://tower.im/projects/f36d2393669943619e47cdded84d1ffa/docs/a543fe2027d2401282b7474df57a89bd/'
        },
        {
            name: '兑换接口',
            url: 'http://api.seentime.com/buyers/:buyerId/exchange',
            type: 'GET',
            status: 1,
            detailUrl: 'https://tower.im/projects/f36d2393669943619e47cdded84d1ffa/docs/c402f1cd0226430bbcaebb7eee8b6b0a/'
        },
        {
            name: '获取消息列表',
            url: 'http://api.seentime.com/buyers/:buyerId/messages',
            type: 'GET',
            status: 1,
            detailUrl: 'https://tower.im/projects/f36d2393669943619e47cdded84d1ffa/docs/3c09fd25702e4ee5a572b41fa3388672/'
        },
        {
            name: '获取消息详情',
            url: 'http://api.seentime.com/buyers/:buyerId/messages/:messageItemId/details',
            type: 'GET',
            status: 1,
            detailUrl: 'https://tower.im/projects/f36d2393669943619e47cdded84d1ffa/docs/74aa6279662945cdb0a3d604476c2f8e/'
        }

    ];

    var sellers = [
        {
            name: '商家注册',
            url: 'http://api.seentime.com/seller/register',
            type: 'POST',
            status: 1,
            detailUrl: 'https://tower.im/projects/f36d2393669943619e47cdded84d1ffa/docs/4beba8cc68e64e39a07bbf36ee751698/'
        },
        {
            name: '商家登录(通过用户名和密码获取token)',
            url: 'http://api.seentime.com/seller/authorize',
            type: 'POST',
            status: 0,
            detailUrl: 'https://tower.im/projects/f36d2393669943619e47cdded84d1ffa/docs/ea0a624b32554b3bbe8fb56bfa7cbd56/'
        },
        {
            name: '商家登录(通过refreshToken获取token)',
            url: 'http://api.seentime.com/seller/authorize',
            type: 'POST',
            status: 0,
            detailUrl: 'https://tower.im/projects/f36d2393669943619e47cdded84d1ffa/docs/5fda3a11c8e0498f9932af6d28e8f172/'
        },
        {
            name: '获取商家信息(使用token)',
            url: 'http://api.seentime.com/sellers/info',
            type: 'GET',
            status: 0,
            detailUrl: ''
        },
        {
            name: '更新商家信息',
            url: 'http://api.seentime.com/sellers/:sellerId',
            type: 'PATCH',
            status: 0,
            detailUrl: 'https://tower.im/projects/f36d2393669943619e47cdded84d1ffa/docs/b395919b54ab4f35b10430d8d63b4309/'
        },
        {
            name: '商家商品列表',
            url: 'http://api.seentime.com/sellers/:sellerId/items',
            type: 'GET',
            status: 0,
            detailUrl: ''
        },
        {
            name: '商家创建商品',
            url: 'http://api.seentime.com/sellers/:sellerId/items',
            type: 'POSTt',
            status: 0,
            detailUrl: ''
        },
        {
            name: '商家更新商品',
            url: 'http://api.seentime.com/sellers/:sellerId/items',
            type: 'PATCH',
            status: 0,
            detailUrl: ''
        },
        {
            name: '商家创建任务活动',
            url: 'http://api.seentime.com/sellers/:sellerId/tasks',
            type: 'POST',
            status: 0,
            detailUrl: 'https://tower.im/projects/f36d2393669943619e47cdded84d1ffa/docs/d84ba555008e4cb9b94d44908e9240d7/'
        },
        {
            name: '商家更新任务活动',
            url: 'http://api.seentime.com/sellers/:sellerId/tasks',
            type: 'PATCH',
            status: 0,
            detailUrl: ''
        },
        {
            name: '商家的任务订单列表',
            url: 'http://api.seentime.com/sellers/:sellerId/task_orders',
            type: 'GET',
            status: 0,
            detailUrl: 'https://tower.im/projects/f36d2393669943619e47cdded84d1ffa/docs/69e4d8537eb14e898f79aac695834dda/'
        },
        {
            name: '商家的财务记录',
            url: 'http://api.seentime.com/sellers/:sellerId/finance_records',
            type: 'GET',
            status: 0,
            detailUrl: ''
        },

    ];

    this.body = yield render('index', {
        mipinrs: mipinrs,
        publics: publics,
        sellers: sellers,
        buyers: buyers
    });

};
