var php = require('phpjs');
var base = require('./base');
var error = require('./error');
var _ = require('underscore');
var useragent = require('express-useragent');

// ==================私有属性=========================

/**
 * sign密钥
 *
 * @property signKey
 * @type {Object}
 */
var signKey = {
    aos: 'Xc65LhOx7DH5NlvR0tgMm01TFYyLhGs5',
    ios: 'okLg9VhVTJSii12CvBYnfVCK8x1R0HZU'
};

// ==================公有属性=========================
/**
 *  config
 *
 * @property config
 * @type {Object}
 */
exports.config = {
    ios: {
        initPoints: 0, // 账户初始化积分
        inviteFartherPoints: 0, // 邀请成功师傅获得积分
        inviteSonPoints: 0, // 邀请成功徒弟获得积分
        ratio: 0.10, //分成比例
        isShow: true, //是否弹窗
        host: 'http://xiaoqianbao.github.io'
    },
    aos: {
        initPoints: 100, // 账户初始化积分
        inviteFartherPoints: 100, // 邀请成功师傅获得积分
        inviteSonPoints: 100, // 邀请成功徒弟获得积分
        ratio: 0.10, //分成比例
        isShow: true, //是否弹窗
        host: 'http://xiaoqianbao.github.io'
    }

};

exports.wall = {
    youmi: 1,
    duomeng: 2,
    lime: 3,
    dianru: 4,
    midi: 5,
    anwo: 6,
    dianle: 7,
    guomeng: 8,
    wanpu: 9,
    yijifen: 10,
    chukong: 11,
    yinggao: 12,
    qumi: 13,
    jusha: 14,
    juzhang: 15
};

// ==================私有方法=========================

/**
 * 返回数据
 *
 * @method res
 * @param {Object} res 响应对象
 * @param {Object} data 返回的对象
 * @return Null
 */
exports.res = function(res, data) {
    res.charSet('utf-8');
    res.json(data);
    res.end();
};

/**
 * 异常处理
 *
 * @method throwError
 * @param {Object} res 响应对象
 * @param {String} type 错误码类型
 * @return null
 */
exports.throwError = function(res, type) {

    var data = {
        c: type,
        msg: error.type[type]
    };

    base.res(res, data);
};

/**
 * 解析url?k1=v1&k2=v2格式的字符串
 *
 * @method _parseUrlParams
 * @param {String} url
 * @return {Object} params 解析完的key-value对象
 */
_parseUrlParams = function(url) {

    var params = {};

    var parseUrl = php.parse_url(url);
    var queryArray = php.explode('&', parseUrl.query);

    for(var i=0; i< queryArray.length; i++) {
        var a = php.explode('=', queryArray[i]);
        params[a[0]] = php.urldecode(a[1]);
    }

    return params;
};


/**
 * 签名生成算法
 *
 * @method _getSignature
 * @param {Object} params 参数对象
 * @param {String} accessSecret 签名密钥
 * @return {String} 签名
 */
_getSignature = function(params, accessSecret) {


    var str = ''; //待签名字符串

    //先将参数以其参数名的字典序升序进行排序
    params = php.ksort(params);

    for(var p in params) {

        //为key/value对生成一个key=value格式的字符串，并拼接到待签名字符串后面
        str = str + p + '=' + php.urlencode(params[p]);
    }

    //将签名密钥拼接到签名字符串最后面
    str =  str + accessSecret;

    //通过md5算法为签名字符串生成一个md5签名，该签名就是我们要追加的sign参数值
    return php.md5(str);
    // return str;

};


/**
 * 检查签名，注意去掉sign
 *
 * @method checkSignature
 * @param {Object} req 请求对象
 * @param {Object} res 响应对象
 * @return null
 */
exports.checkSignature = function (req, res) {


    var platform = base.getPlatform(req);
    var accessSecret;

    if(platform === '8') {
        accessSecret = signKey.aos;
    } else if(platform === '16') {
        accessSecret = signKey.ios;
    }


    // 签名参数
    var sign = req.params.sign;

    // 参数中不存在签名
    if(!req.params.sign) {

        base.throwError(res, '-1038');
    // 存在sign参数
    } else {

        // 去掉sign参数
        delete req.params.sign;

        // 签名不正确
        if (sign !== _getSignature(req.params, accessSecret)) {


            // base.res(res,_getSignature(req.params, accessSecret) );
            base.throwError(res, '-1038');
        }
    }

};

/**
 * 检查request参数
 *
 * @method checkParams
 * @param {Object} req 请求对象
 * @param {Object} res 响应对象
 * @param {Array} checkArray 检查的参数数组
 * @return null
 */
exports.checkParams = function(req, res, checkArray) {

    if (_.isArray(checkArray)) {

        var equal = true; // 假设参数正确
        _.each(checkArray, function(i) {

            if(!_.has(req.params, i)) {
                equal = false;
            }
        });

        if(!equal) {
            // 参数不正确
            base.throwError(res, '-1030');
        }
    } else {

        // 参数不正确
        base.throwError(res, '-1030');
    }
}


/**
 * 获取设备平台
 *
 * @method checkSignature
 * @param {Object} req 请求对象
 * @return {Number}
 */

exports.getPlatform = function(req) {

    var source = req.headers['user-agent'],
    ua = useragent.parse(source);

    if(ua.isMobile && ua.isAndroid) {

        return '8'; // Android

    } else if(ua.isMobile && (ua.isiPad || ua.isiPod || ua.isiPhone)) {

        return '16'; // iOS

    } else {
        return '100'; // others
    }

};
