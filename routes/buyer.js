var _ = require('underscore');
var db = require('../models');
var crawler = require('../libs/crawler');
var forvy = require('forvy');
var helper = require('../libs/helper.js');
var moment = require('moment');
var php = require('phpjs');
var qqwry = require('lib-qqwry').init(); //调用并初始化，普通机器初始需要70毫秒左右;

// 获取用户信息
exports.getBuyerInfo = function *(next) {
  var accessToken = helper.getToken(this);
  var buyer = yield db.OauthAccessToken.find({ where: { accessToken: accessToken } });
  this.body = yield db.Buyer.find({ include: [db.Taobao], where: { id: buyer.buyerId } });
};

// 获取用户信息
exports.getBuyer = function *() {

  this.body = yield db.Buyer.find({ include: [db.Taobao], where: { id: this.params.buyerId } });
};

// 修改用户信息
exports.updateBuyer = function *() {
  var body = this.request.body || {};
  yield db.Buyer.update(body, { where:{ id: this.params.buyerId } });
  this.body = yield db.Buyer.find({ where:{ id: this.params.buyerId } });
};

// 获取商品列表
exports.getItems = function *() {

  var query = this.query;

  // 获取分页查询条件
  var condition = helper.getCondition({
    where: { buyerId: this.params.buyerId },
    page: query.page,
    perPage: query.perPage
  });

  var data = yield db.Item.findAndCountAll(condition);
  this.set({ 'X-Pagination-Total-Count': data.count });
  this.body = data.rows;
};

// 创建商品
exports.createItem = function *() {

  var body = this.request.body || {};
  body.buyerId = this.params.buyerId;

  // 创建商品
  var newItem = yield db.Item.create(body);
  newItem.id = yield db.Item.max('id');
  this.body = newItem;
  this.body = 'newItem';
};

// 修改商品
exports.updateItem = function *() {
  var body = this.request.body || {};
  yield db.Item.update(body, { where:{ id: this.params.itemId } });
  this.body = yield db.Item.find({ where:{ id: this.params.itemId } });
};

// 红包订单
exports.getHongbaoOrders = function *() {

  var status;
  var buyerId = this.params.buyerId;
  var query = this.query;

  switch (query.type) {
    case '1':
      status = [10, 20, 21, 22, 23, 40, 42];
      break;
    case '2':
      status = [41];
      break;
    case '3':
      status = [11, 5];
      break;
    default:
      status = [];
      break;
  }

  // 获取分页查询条件
  var condition = helper.getCondition({
    include: [db.Buyer, db.Seller, db.Hongbao, db.Taobao],
    where: { buyerId: buyerId, status: status },
    page: query.page,
    perPage: query.perPage
  });

  // 查询数据
  var data = yield db.HongbaoOrder.findAndCountAll(condition);

  this.set({ 'X-Pagination-Total-Count': data.count });

  var hongbaoOrders = []; // 红包订单

  _.each(data.rows, function(value, key) {
    hongbaoOrders.push({
      itemUrl: value.Hongbao.itemUrl,
      picUrl: value.Hongbao.picUrl,
      title: value.Hongbao.title,
      price: value.Hongbao.price,
      delivery: value.Hongbao.delivery,
      source: value.Hongbao.source,
      reward: value.Hongbao.reward,
      guarantyMoney: value.Hongbao.guarantyMoney,
      amount: value.Hongbao.amount,
      remain: value.Hongbao.remain,
      status: value.status,
      createdAt: value.createdAt

    });
  });

  this.body = hongbaoOrders;
};

// 获取task订单列表
exports.getTaskOrders = function *() {

  var query = this.query;

  // 获取分页查询条件
  var condition = helper.getCondition({
    include: [db.Task, db.Buyer, db.Taobao, db.Seller],
    where: {
      buyerId: this.params.buyerId
    },
    page: query.page,
    perPage: query.perPage
  });

  // 获取不同状态的订单
  if (query.statusType == 1) condition.where.status = [10, 21, 40, 20, 22, 23, 41]; // 进行中
  if (query.statusType == 2) condition.where.status = 42; // 已完成
  if (query.statusType == 3) condition.where.status = 11; // 已失效

  var data = yield db.TaskOrder.findAndCountAll(condition);
  this.set({ 'X-Pagination-Total-Count': data.count });
  this.body = data.rows;
};

// 创建task订单
exports.createTaskOrder = function *() {
  var body = this.request.body || {};
  body.buyerId = this.params.buyerId;
  var order = yield db.TaskOrder.find({ where: { buyerId: body.buyerId, taskId: body.taskId } });
  var task = yield db.Task.find({ where: { id: body.taskId } });

  if (_.isEmpty(order)) {

    // 保存taskOrder
    var newOrder = yield db.TaskOrder.create(body);

    newOrder.id = yield db.TaskOrder.max('id');

    // 创建消息
    var message = {
      buyerId: this.params.buyerId,
      taskOrderId: newOrder.id,
      messageItemId: newOrder.id,
      type: 10,
      title: task.title,
      content: '您已成功申请该商品的试用，商家正在火速审核中，请耐心等待',
      picUrl: task.picUrl
    };
    var newMessege = yield db.Message.create(message);

    this.body = newOrder;
  } else {
    this.body = { code: 1, message: '你已申请过该试用' };
  }
};

// task订单审核操作
exports.updateTaskOrder = function *() {

  var body = this.request.body || {};
  yield db.TaskOrder.update(body, { where:{ id: this.params.taskOrderId } });
  this.body = yield db.TaskOrder.find({ where:{ id: this.params.taskOrderId } });
};

// 获取单个红包订单
exports.getHongbaoOrder = function *() {

  var data = yield db.HongbaoOrder.find({
    include: [db.Buyer, db.Seller, db.Hongbao, db.Taobao],
    where: { buyerId: this.params.buyerId, id: this.params.hongbaoOrderId }
  });

  this.body = {
    orderNumber: data.orderNumber,
    status: data.status,
    createdAt: data.createdAt,
    itemUrl: data.Hongbao.itemUrl,
    picUrl: data.Hongbao.picUrl,
    title: data.Hongbao.title,
    price: data.Hongbao.price,
    delivery: data.Hongbao.delivery,
    reward: data.Hongbao.reward,
    guarantyMoney: data.Hongbao.guarantyMoney,
    amount: data.Hongbao.amount,
    remain: data.Hongbao.remain,
    startTime: data.Hongbao.startTime,
    endTime: data.Hongbao.endTime
  };
};

// 获取淘宝号
exports.getTaobaos = function *() {
  this.body = yield db.Taobao.findAll({
    where: { buyerId: this.params.buyerId, isDeleted: 0 },
    order: 'createdAt DESC'
  });
};

// 创建淘宝号
exports.createTaobao = function *() {

  var taobao = yield crawler.getTaobao(this.request.body.name);

  this.body = taobao;

  // 获取淘宝账号信息
  if (!taobao) {
    this.body = { code: 404, message: '淘宝账号不存在' };
  } else {

    taobao.buyerId = this.params.buyerId;

    var data = yield db.Taobao.find({ where: { name: taobao.name } });

    if (!_.isEmpty(data)) {
      this.body = { code: 400, message: '此淘宝已经存在' };
    } else {

      var ipL = qqwry.searchIP(this.ip); //查询IP信息
      taobao.ip =  this.ip;
      taobao.region = ipL.Country;
      taobao.ipType = ipL.Area;

      // 保存淘宝账号
      var newTaobao = yield db.Taobao.create(taobao);
      newTaobao.id = yield db.Taobao.max('id'); // 这个可能会有问题
      this.body = newTaobao;
    }
  }

};

exports.deleteTaobao = function *() {
  yield db.Taobao.update({ isDeleted: 1 }, { where:{ id: this.params.taobaoId } });
  this.body = { code: 1, message: '删除成功' };
};

// 兑换接口
exports.exchange = function *() {
  var buyerId = this.params.buyerId;
  var buyer = yield db.Buyer.find({ where: { id: buyerId } });
  this.body = buyer;
  var url = _buildCreditAutoLoginRequest('2ewpWsXUTNZdKkB4pqThGSD1shBM', '2uYqGXyTDyfUBp7Zrx39XsS5VqhB', buyerId, buyer.balance);
  this.body = { url: url };

  /*
  *  生成自动登录地址
  *  通过此方法生成的地址，可以让用户免登陆，进入积分兑换商城
  */
  function _buildCreditAutoLoginRequest(appKey, appSecret, uid, credits) {

    var url = 'http://www.duiba.com.cn/autoLogin/autologin?';
    var timestamp = (new Date()).valueOf();

    var params = {
          uid: uid,
          credits: credits,
          appSecret: appSecret,
          appKey: appKey,
          timestamp: timestamp
        };

    var sign = _sign(params);

    //生成签名
    function _sign(params) {
      var str = ''; //待签名字符串
      params = php.ksort(params);

      for (var p in params) {

        str = str + params[p];
      }

      return php.md5(str);
    };

    url = url + 'timestamp=' + timestamp + '&uid=' + uid + '&credits=' + credits + '&appKey=' + appKey + '&sign=' + sign;
    return url;
  }
};

// 获取消息列表
exports.getMessages = function *() {
  // this.body = yield db.Message.findAll();
  this.body = yield db.Message.findAll({
    // where: {buyerId: this.params.buyerId, isDeleted: 0},
    group: 'taskOrderId',
    order: 'id DESC'
  });
};

// 获取消息详情列表
exports.getMessageDetails = function *() {

  var buyerId = this.params.buyerId;
  var messageItemId = this.params.messageItemId;
  var query = this.query;

  // this.body = yield db.Message.findAll();
  this.body = yield db.Message.findAll({
    where: { messageItemId: messageItemId },
    order: 'id DESC'
  });
};

// 获取装园
exports.getCollections = function *() {

  var query = this.query;

  // 获取分页查询条件
  var condition = helper.getCondition({
    include: [db.Item],
    where: {
      buyerId: this.params.buyerId
    },
    page: query.page,
    perPage: query.perPage
  });

  var data = yield db.Collection.findAndCountAll(condition);
  this.set({ 'X-Pagination-Total-Count': data.count });
  this.body = data.rows;
};
