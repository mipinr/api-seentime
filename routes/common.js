'use strict';

/*
 * 不用授权的接口 by forsigner
 */
var php = require('phpjs');
var moment = require('moment');
var _ = require('underscore');
_.str = require('underscore.string');
var crypto = require('crypto');
var forvy = require('forvy');
var db = require('../models');
var helper = require('../libs/helper');
var crawler = require('../libs/crawler');
var oauthConfig = require('../libs/oauthConfig');
var qq = require('../libs/qq');
var weibo = require('../libs/weibo');

var request = require('request');
var url = require('url');
var querystring = require('querystring');
var iconv = require('iconv-lite');
var BufferHelper = require('bufferhelper');

exports.jds = function*() {

  var query = this.query;

  var today = yield db.Item.max('updatedAt'); // 把数据库最新的一天左右最近一天

  // 获取分页查询条件
  var condition = helper.getCondition({
    page: query.page,
    perPage: query.perPage
  });

  // AppType
  if (this.get('App-Type')) {
    condition.where.appType = this.get('App-Type');
  }

  // 如果存在channel
  if (query.channel) {
    condition.where.channel = query.channel;
  }

  // 如果存在categoryId
  if (query.categoryId) {
    condition.where.categoryId = query.categoryId;
  }

  // 如果不存在where就删掉它
  if (_.isEmpty(condition.where)) {
    delete condition.where;
  }

  var data = yield db.Item.findAndCountAll(condition);
  var rows = { c: 0, d: data.rows };

  this.set({ 'X-Pagination-Total-Count': data.count });
  this.body = rows;

};

exports.jd = function*() {

  var d = yield db.Item.find({ where: { id: this.query.gid } });
  var data = { c: 0, d: d };
  this.body = data;

};

// 程序配置接口
exports.getConfig = function*() {
  // var config = yield db.Config.find(1);
  var config = yield db.Config.findAll();

  // this.body = config;
  this.body = yield db.Seller.findAll({ include: [db.Item] });
};

// 获取baner列表
exports.getBanners = function*() {

  this.body = yield db.Banner.findAll({
    where: {
      appType: this.get('App-Type'),
      isShow: true
    }
  });

};

// 获取入口列表
exports.getEntrys = function*() {

  this.body = yield db.Entry.findAll();
};

// 用户注册，分各种类型
exports.buyerRegister = function*() {

  var body = this.request.body;

  switch (body.registerType) {

    // 手机注册
    case '1':

      // 检查用户
      var buyer = yield db.Buyer.find({
        where: {
          phone: body.phone
        }
      });

      // 手机是否被注册
      if (!_.isEmpty(buyer)) {
        this.body = { code: 422, message: '此手机已被注册' };
      } else {

        // 生成密码
        var password = helper.generatePassword(body.password);

        body.password = password;
        body.nickname = body.password.substr(-4, 4) + body.phone.substr(-4, 4);
        body.avatar = 'http://gd1.alicdn.com/imgextra/i1/2207718142/TB29nOzbXXXXXbHXXXXXXXXXXXX_!!2207718142.jpg_160x160.jpg';

        // 保存买家
        yield db.Buyer.create(body);
        this.body = yield db.Buyer.find({
          where: {
            phone: body.phone
          }
        });
      }

      break;

    // 邮箱注册
    case '2':

      // 检查用户
      var buyer = yield db.Buyer.find({
        where: {
          email: body.email
        }
      });

      // 手机是否被注册
      if (!_.isEmpty(buyer)) {
        this.body = { code: 400, message: '此邮箱已被注册' };
      } else {

        // 生成密码
        var password = helper.generatePassword(body.password);

        body.password = password;
        body.username = body.email;
        body.nickname = body.nickname;
        body.intro = '发现最美的品牌';
        body.avatar = 'http://gd1.alicdn.com/imgextra/i1/2207718142/TB29nOzbXXXXXbHXXXXXXXXXXXX_!!2207718142.jpg_160x160.jpg';

        // 保存
        db.Buyer.create(body);
        this.body = db.Buyer.find({
          where: {
            email: body.email
          }
        });

      }

      break;

    // QQ注册
    case '3':

      // 获取accessToken
      var accessToken = yield qq.getAccessToken(body.code);

      // 获取openid
      var openid = yield qq.getOpenid(accessToken);

      // 检查改openid是否存在
      var buyer = yield db.Buyer.find({ where: { openid: openid } });

      // yes
      if (!_.isEmpty(buyer)) {

        // 我们自己的token
        var seentimeToken = yield helper.createToken(openid, oauthConfig.qq.defaultPassword);

        // 把用用户信息和token合并
        this.body = _.extend(seentimeToken, buyer.dataValues);
      } else {

        // 获取用户信息
        var user = yield qq.getUserInfo(accessToken, openid);

        // 生成密码，所有QQ登录设置默认密码
        var password = helper.generatePassword(oauthConfig.qq.defaultPassword);

        if (user.gender == '男') {
          var gender = 1;
        } else if (user.gender == '男') {
          var gender = 2;
        } else {
          var gender = 0;
        }

        // 注册用户的信息
        var data = {
          password: password,
          avatar: user.figureurl_qq_2,
          nickname: user.nickname,
          gender: gender,
          city: user.city,
          province: user.province,
          phone: openid,
          username: openid,
          openid: openid,
          registerType: 3
        };

        // 保存买家
        yield db.Buyer.create(data);

        // 我们自己的token
        var seentimeToken = yield helper.createToken(openid, oauthConfig.qq.defaultPassword);

        var newBuyer = yield db.Buyer.find({
          where: {
            openid: openid
          }
        });

        this.body = _.extend(seentimeToken, newBuyer.dataValues);
      }

      break;

    // 微博注册
    case '4':

      // 获取accessToken 和 openid
      var tokenInfo = yield weibo.getAccessTokenAndOpenid(body.code);

      // accessToken
      var accessToken = tokenInfo.access_token;

      // 获取openid
      var openid = tokenInfo.uid;

      // 检查改openid是否存在
      var buyer = yield db.Buyer.find({ where: { openid: openid } });

      // yes
      if (!_.isEmpty(buyer)) {

        // 我们自己的token
        var seentimeToken = yield helper.createToken(openid, oauthConfig.weibo.defaultPassword);

        // 把用用户信息和token合并
        this.body = _.extend(seentimeToken, buyer.dataValues);
      } else {

        // 获取用户信息
        var user = yield weibo.getUserInfo(accessToken, openid);

        // 生成密码，所有微博登录设置默认密码
        var password = helper.generatePassword(oauthConfig.weibo.defaultPassword);

        // 注册用户的信息
        var data = {
          password: password,
          avatar: user.avatar_hd,
          nickname: user.screen_name,
          gender: user.gender == 'm' ? 1 : 2,
          province: user.location.split(' ')[0],
          city: user.location.split(' ')[1],
          phone: openid,
          username: openid,
          openid: openid,
          registerType: 4
        };

        // 保存买家
        yield db.Buyer.create(data);

        // 我们自己的token
        var seentimeToken = yield helper.createToken(openid, oauthConfig.weibo.defaultPassword);

        var newBuyer = yield db.Buyer.find({
          where: {
            openid: openid
          }
        });

        this.body = _.extend(seentimeToken, newBuyer.dataValues);
      }

      break;

    default:
      this.body = { code: 400, message: 'type error' };
  }

};

// 重置密码
exports.buyerResetPassword = function*() {
  var body = this.request.body;
  var buyer = yield db.Buyer.find({ where: { phone: body.phone } });

  if (_.isEmpty(buyer)) {
    this.body = { code: 422, message: '手机不存在' };
  } else {

    // 生成密码
    var password = helper.generatePassword(body.password);

    yield db.Buyer.update({ password: password }, {
      where: {
        phone: body.phone
      }
    });
    this.body = yield db.Buyer.find({
      where: {
        phone: body.phone
      }
    });
  }
};

// 商家注册
exports.sellerRegister = function*() {

  var body = this.request.body;
  console.log(body);

  switch (body.registerType) {

    // 手机注册
    case '1':

      // 检查用户
      var seller = yield db.Seller.find({
        where: {
          phone: body.phone
        }
      });

      // 手机是否被注册
      if (!_.isEmpty(seller)) {
        this.body = { code: 422, message: '此手机已被注册' };
      } else {

        // 生成密码
        var password = helper.generatePassword(body.password);

        body.password = password;

        body.nickname = body.password.substr(-4, 4) + body.phone.substr(-4, 4);
        body.avatar = 'http://gd1.alicdn.com/imgextra/i1/2207718142/TB29nOzbXXXXXbHXXXXXXXXXXXX_!!2207718142.jpg_160x160.jpg';

        // 保存商家
        yield db.Seller.create(body);
        this.body = yield db.Seller.find({
          where: {
            phone: body.phone
          }
        });
      }

      break;

    // 邮箱注册
    case '2':

      // 检查用户
      var seller = yield db.Seller.find({
        where: {
          email: body.email
        }
      });

      // 手机是否被注册
      if (!_.isEmpty(seller)) {
        this.body = { code: 400, message: '此邮箱已被注册' };
      } else {

        // 生成密码
        var password = helper.generatePassword(body.password);

        body.password = password;

        body.nickname = _.str.strLeft(body.email, '@');
        body.avatar = 'http://gd1.alicdn.com/imgextra/i1/2207718142/TB29nOzbXXXXXbHXXXXXXXXXXXX_!!2207718142.jpg_160x160.jpg';

        // 保存
        db.Seller.create(body);
        this.body = db.Seller.find({
          where: {
            email: body.email
          }
        });

      }

      break;

    default:
      this.body = { code: 400, message: 'type error' };
  }

};

// 获取用户列表
exports.getUsers = function*() {

  var query = this.query;

  var condition = helper.getCondition({
    page: query.page,
    perPage: query.perPage
  });

  var data = yield db.Buyer.findAndCountAll(condition);
  this.set({ 'X-Pagination-Total-Count': data.count });
  this.body = data.rows;

};

// 获取单个用户
exports.getUser = function*() {
  this.body = yield db.Buyer.find({
    where: {
      id: this.params.userId
    }
  });
};

// 获取商品列表
exports.getItems = function*() {

  var query = this.query;

  var today = yield db.Item.max('updatedAt'); // 把数据库最新的一天左右最近一天

  if (query.buyerId) {
    // 获取分页查询条件
    var condition = helper.getCondition({
      include: [
        {
          model: db.Buyer,
          where: {
            id: query.buyerId
          }
        },
        {
          model: db.Brand
        }
      ],
      attributes: ['id', 'title', 'picUrl', 'itemUrl', 'promoPrice', 'originPrice', 'discount', 'sellCount', 'favCount', 'createdAt', 'updatedAt'],
      page: query.page,
      perPage: query.perPage
    });

    var data = yield db.Item.findAndCountAll(condition);
    this.set({ 'X-Pagination-Total-Count': data.count });
    this.body = data.rows;

  } else if (query.topicId) {
    // 获取分页查询条件
    var condition = helper.getCondition({
      include: [
        {
          model: db.Topic,
          where: {
            id: query.topicId
          }
        },

      // {
      //     model: db.Brand,
      // }
      ],
      attributes: ['id', 'title', 'picUrl', 'promoPrice', 'itemUrl', 'originPrice', 'discount', 'sellCount', 'favCount', 'createdAt', 'updatedAt'],
      page: query.page,
      perPage: query.perPage
    });

    var data = yield db.Item.findAndCountAll(condition);
    this.set({ 'X-Pagination-Total-Count': data.count });
    this.body = data.rows;

  } else {
    // 获取分页查询条件
    var condition = helper.getCondition({
      include: [
        {
          model: db.Brand,
          attributes: ['id', 'name', 'logoUrl']
        }
      ],
      attributes: ['id', 'title', 'picUrl', 'itemUrl', 'promoPrice', 'originPrice', 'discount', 'sellCount', 'favCount', 'createdAt', 'updatedAt'],
      page: query.page,
      perPage: query.perPage
    });

    // AppType
    if (this.get('App-Type')) {
      condition.where.appType = this.get('App-Type');
    }

    // 是否popular
    if (query.isPopular == 1) {
      condition.limit = 8;
    }

    // 是否是查询今日
    if (query.isToday == 1) {
      condition.where.updatedAt = {
        between: [moment(today).format('YYYY-MM-DD'), moment().add(1, 'days').format('YYYY-MM-DD')]
      };
    }

    // 如果存在channel
    if (query.channel) {
      condition.where.channel = query.channel;
    }

    // 如果存在source
    if (query.source) {
      condition.where.source = query.source;
    }

    // 如果存在categoryId
    if (query.categoryId) {
      condition.where.categoryId = query.categoryId;
    }

    if (query.primaryCategoryId) {
      if (query.primaryCategoryId === '101') {
        condition.where.categoryId = { $between: [1, 38] };
      }

      if (query.primaryCategoryId === '102') {
        condition.where.categoryId = { $between: [39, 53] };
      }

      if (query.primaryCategoryId === '103') {
        condition.where.categoryId = { $between: [54, 69] };
      }

      if (query.primaryCategoryId === '104') {
        condition.where.categoryId = { $between: [79, 94] };
      }

      if (query.primaryCategoryId === '105') {
        condition.where.categoryId = { $between: [95, 127] };
      }

      if (query.primaryCategoryId === '106') {
        condition.where.categoryId = { $between: [128, 190] };
      }
    }

    // 如果存在brandId
    if (query.brandId) {
      condition.where.brandId = query.brandId;
    }

    // 如果存在seckillId
    if (query.seckillId) {
      condition.where.seckillId = query.seckillId;
    }

    // 如果不存在where就删掉它
    if (_.isEmpty(condition.where)) {
      delete condition.where;
    }

    var data = yield db.Item.findAndCountAll(condition);
    this.set({ 'X-Pagination-Total-Count': data.count });
    this.body = data.rows;

  }

};

// =createItem 创建商品
exports.createItem = function*() {

  var body = this.request.body;
  var item = yield db.Item.find({ where: { goodId: body.goodId } });

  // 商品是否存在
  if (!_.isEmpty(item)) {
    this.body = '此商品已经存在';
  } else {

    // 保存商品
    db.Item.create(body);
    this.body = yield db.Item.find({
      where: {
        goodId: params.goodId
      }
    });
  }
};

// =getItem 获取单个商品
exports.getItem = function*() {
  this.body = yield db.Item.find({
    include: [
      {
        model: db.Brand,
        attributes: ['id', 'name', 'logoUrl', 'itemCount', 'followerCount']
      }
    ],
    attributes: ['id', 'title', 'picUrl', 'itemUrl', 'promoPrice', 'originPrice', 'discount', 'favCount', 'createdAt', 'updatedAt'],
    where: {
      id: this.params.itemId
    }
  });
};

// 类目
exports.getCategorys = function*() {

  var appType = this.get('App-Type') || 2;
  var type = this.query.type || '1';

  // 如果类目需要分组
  if (type == '1') {

    this.body = yield db.Category.findAll({
      group: 'primaryCategoryId',
      where: {
        appType: appType
      },
      attributes: ['primaryCategoryId', 'primaryCategoryName', 'englishName', 'primaryCategoryIconClass']

    });

  // 如果类目不需要分组
  } else if (type == '2') {

    this.body = yield db.Category.findAll({
      group: 'secondaryCategoryId',
      where: {
        appType: appType
      },
      attributes: ['secondaryCategoryId', 'secondaryCategoryName', 'secondaryCategoryIconClass']
    });

  } else {
    this.body = yield db.Category.findAll({
      where: {
        appType: appType
      }
    });
  }

};

exports.getItemTags = function*() {
  this.body = yield db.ItemTag.findAll({
    where: {
      appType: this.query.appType
    }
  });
};

// 获取商品信息
exports.getItemInfo = function*() {

  var body = this.request.body;

  // this.body = {code: 2033, message: '商品url无效'};

  if (helper.checkItemUrl(body.itemUrl)) {

    // 获取商品信息
    this.body = yield forvy.getItemInfo(body.itemUrl);
  } else {

    this.status = 422;
    this.body = { code: 2033, message: '商品url无效' };
  }

};

// 获取商品历史价格
exports.getItemHistoryPrice = function*() {

  var body = this.request.body;

  if (helper.checkItemUrl(body.itemUrl)) {
    // 获取商品信息
    this.body = yield forvy.getItemHistoryPrice(body.itemUrl);
  } else {
    this.status = 422;
    this.body = { code: 2033, message: '商品url无效' };
  }

};

exports.tag = function*() {

  // 载入模块
  var Segment = require('node-segment').Segment;

  // 创建实例
  var segment = new Segment();

  // 使用默认的识别模块及字典，载入字典文件需要1秒，仅初始化时执行一次即可
  segment.useDefault();

  // 开始分词
  var result = segment.doSegment('这是一个基于Node.js的中文分词模块。');

  var items = yield db.Item.findAll({ where: { id: 186 } });

  _.each(items, function(value, key) {
    // arr = arr + value.w + ',';

    var arr = '';
    _.each(segment.doSegment(value.title).sort(), function(value) {
      arr = arr + value.w + ',';
    });

    db.Item.update({ tag: arr }, { where: { id: 186 } });
  });

  this.body = items;
};

// 搜索
exports.search = function*() {

  var query = this.query;

  // 1->item
  // 2->brand
  // 3->topic
  // 4->seckill

  // 获取分页查询条件
  switch (query.type) {
    case '1':
      var condition = helper.getCondition({
        page: query.page,
        perPage: query.perPage,
        where: db.Sequelize.or(
          { title: { like: '%' + this.query.q + '%' } },
          { originTitle: { like: '%' + this.query.q + '%' } }
        )

      });

      // AppType
      if (this.get('App-Type')) {
        condition.where.appType = this.get('App-Type');
      }

      var data = yield db.Item.findAndCountAll(condition);
      this.set({ 'X-Pagination-Total-Count': data.count });
      this.body = data.rows;
      break;
    case '2':

      var condition = helper.getCondition({
        page: query.page,
        perPage: query.perPage,
        where: db.Sequelize.or(
          { name: { like: '%' + this.query.q + '%' } },
          { story: { like: '%' + this.query.q + '%' } }
        )

      });

      // AppType
      if (this.get('App-Type')) {
        condition.where.appType = this.get('App-Type');
      }

      var data = yield db.Brand.findAndCountAll(condition);
      this.set({ 'X-Pagination-Total-Count': data.count });
      this.body = data.rows;
      break;
    case '3':

      var condition = helper.getCondition({
        page: query.page,
        perPage: query.perPage,
        where: db.Sequelize.or(
          { name: { like: '%' + this.query.q + '%' } },
          { story: { like: '%' + this.query.q + '%' } }
        )

      });

      // AppType
      if (this.get('App-Type')) {
        condition.where.appType = this.get('App-Type');
      }

      var data = yield db.Topic.findAndCountAll(condition);
      this.set({ 'X-Pagination-Total-Count': data.count });
      this.body = data.rows;
      break;
    default:
      break;
  }
};
