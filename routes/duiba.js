var db = require('../models');
// var base = require('./base');
var php = require('phpjs');
var moment = require('moment');


//==================兑吧接口 =========================
//=CreditQuery 余额查询
exports.creditQuery = function *() {

    // base.checkParams(res, ['userId', 'appKey', 'timestamp'], req.params);


    db.User.find({where: {id: req.params.uid}})
    .on('success', function(data) {

        var data = {
            status: 'ok',
            message: '查询成功',
            data: {
                credits: data.totalPoints - data.expensePoints,
                phone: data.phone,
                alipay: data.alipay,
                qq: data.qq
            }
        };

        base.res(res, data);

    }).on('fail', function() {

        var data = {
            status: 'fail',
            message:'出错了',
            errorMessage:'数据库出错'
        };

        base.res(res, data);
    });
}



//=parseCreditConsume 兑换
exports.creditConsume = function *() {

    var userId = req.params.uid,
        orderNum = req.params.orderNum,
        credits = req.params.credits,
        type = req.params.type,
        description = req.params.description,
        facePrice = req.params.facePrice,
        actualPrice = req.params.actualPrice,
        alipay = req.params.alipay,
        phone = req.params.phone,
        qq = req.params.qq,
        waitAudit = req.params.waitAudit,
        ip = req.params.ip;

    if(!userId) {

        var data = {
            status: 'fail',
            message: '参数不正确',
            errorMessage:'',
            credits: credits
        };

        base.res(res, data);

    }

    var bizId = moment().format('YYYYMMDDhmmss').toString() + userId;

    // add title
    var title;

    switch(type) {
        case 'qb':
            title = 'Q币' + credits * 0.01 + '个';
            break;

        case 'alipay':

            title = '支付宝' + credits * 0.01 + '元';
            break;

        case 'phonebill':
            title = '手机话费' + credits * 0.01 + '元';
            break;

        case 'coupon':
            title = description;
            break;

        case 'object':
            title = description;
            break;

        default:
            title = '';
    }

    // 保存兑换
    db.OrderRecord.create({
        userId: userId,
        bizId: bizId,
        orderNum: orderNum,
        credits: credits,
        type: type,
        description: description,
        title: title,
        facePrice: facePrice,
        actualPrice: actualPrice,
        alipay: alipay,
        qq: qq,
        phone: phone,
        status: 'create',
        waitAudit: waitAudit,
        ip: ip
    }).on('success', function() {

        db.User
        .find({
            where: {id: userId}
        })
        .on('success', function(user) {

            db.User.update({
                expensePoints: parseInt(user.expensePoints) + parseInt(credits)
            }, {
                where: {id: userId}
            })
            .on('success', function(){

                var data = {
                    status: 'ok',
                    message: '兑换成功',
                    errorMessage:'',
                    data:{
                        bizId: bizId,
                        credits: user.totalPoints - (parseInt(user.expensePoints) + parseInt(credits))
                    }
                };
                base.res(res, data);

            });

        });


    });
};


//=CreditNotify
exports.creditNotify = function *() {

    var userId = req.params.uid;
    var orderNum = req.params.orderNum;
    var bizId = req.params.bizId;
    var success = req.params.success;


    // 兑换成功
    if(success === 'true') {

        db.OrderRecord.update({
            status: 'success'
        }, {
            where: {
                userId: userId,
                orderNum: orderNum,
                bizId: bizId
            }
        })
        .on('success', function(){

            console.log(req.params.success + '-----------------------ok');

            res.send('ok');

        });

    // 兑换失败
    } else if(success === 'false')  {

        // 查找账单
        db.OrderRecord
        .find({
            where: {
                userId: userId,
                orderNum: orderNum,
                bizId: bizId
            }
        })
        .on('success',function(order) {

            // 查找用户
            db.User
            .find({
                where: {
                    id: userId
                }
            })
            .on('success',function(user) {

                // 兑换失败还积分给用户
                db.User.update({
                    expensePoints: parseInt(user.expensePoints) - parseInt(order.credits)
                }, {
                    where: {id: userId}
                })
                .on('success', function(){

                    // 把订单状态变为fail
                    db.OrderRecord.update({
                        status: 'fail'
                    }, {
                        where: {
                            userId: userId,
                            orderNum: orderNum,
                            bizId: bizId
                        }
                    })
                    .on('success', function(){

                        console.log(req.params.success + '-----------------------fail');
                        res.send('ok');


                    });


                });

            });


        });
    }

};


//=orderStatus
exports.orderStatus = function *() {

    var url = 'http://www.duiba.com.cn/status/orderStatus?';
    var timestamp = (new Date()).valueOf();
    var orderNum = '2014100412173265800852084';
    var bizId = '2014100412173259';
    var appKey = '3JHH943YqZX7fjwi3X63Ljw7yvbF';
    var appSecret = 'rzvNuqCMxJGo5HWTeXHEWHinNLF';


    function sign(params) {


        var str = ''; //待签名字符串

        params = php.ksort(params);

        for(var p in params) {

            str = str + params[p];
        }
        return php.md5(str);

    };

    var params = {
      timestamp: timestamp,
      appKey: appKey,
      appSecret: appSecret,
      orderNum: orderNum,
      bizId: bizId

    }

    var sign = sign(params);


    url = url + 'timestamp=' + timestamp + '&orderNum=' + orderNum + '&bizId=' + bizId + '&appKey=' + appKey + '&sign=' + sign;

    // 返回数据
    var data = {
        c: 0,
        d: {
            url: url
        },
        msg: 'done'
    };

    // base.res(res, data);
    res.send(url);
};

