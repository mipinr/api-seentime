'use strict';

/*
 * 不用授权的接口 by forsigner
 */
var php = require('phpjs');
var moment = require('moment');
var _ = require('underscore');
_.str = require('underscore.string');
var request = require('request');
var cheerio = require('cheerio');
var gbk = require('gbk');
var crypto = require('crypto');
var url = require('url');
var querystring = require('querystring');

var db = require('../models');
var oauthConfig = require('../libs/oauthConfig');
var helper = require('../libs/helper');
var crawler = require('../libs/crawler');

var views = require('co-views');
var parse = require('co-body');
var co = require('co');
var iconv = require('iconv-lite');
var BufferHelper = require('bufferhelper');

// 获取文章
exports.getPosts = function * () {
    var query = this.query;
    var condition = helper.getCondition({
        page: query.page,
        perPage: query.perPage,
        order: 'wroteAt DESC'
    });

    var data = yield db.Post.findAndCountAll(condition);
    this.set({'X-Pagination-Total-Count': data.count});
    this.body = data.rows;

};

// 获取文章
exports.createPosts = function * () {

    var host = 'http://forsigner.com'

    // 获取所有文章的url
    var urls = yield new Promise(function(resolve, reject) {

        request({
            method: 'GET',
            uri: 'http://forsigner.com/blog/'
        }, function (error, response, body) {

            var $ = cheerio.load(body);

            var urls = []; // 所有文章的url

            $('.post-item').each(function(i, elem) {
                urls.push(host + $(this).attr('href'));
            });

            resolve(urls);
        });

    });


    // 获取文章内容
    var getPost = function(url) {

       return new Promise(function(resolve, reject) {

            request({
                method: 'GET',
                uri: url
            }, function (error, response, body) {


                body = body.replace(/\n/g, '');
                body = body.replace(/\r/g, '');
                body = body.replace(/\t/g, '');
                body = body.replace(/"/g, "'");

                var $ = cheerio.load(body);

                // 标题
                var title = $('.post-title').text();

                // 日期
                var date = $('.date span').text();
                date = moment(date).format('YYYY-MM-DD');

                //文档内容
                var start = body.indexOf("entry'>");
                var end = body.indexOf("class='pager'");
                var content = body.substring(start + 7, end - 21);

                var data = {
                    title: title,
                    url: url,
                    wroteAt: date,
                    content: content
                }
                resolve(data);
            });

        });

    }

    // 把文章存入数据库
    for(var i = 1; i < urls.length; i++) {

        // 检查是否存在
        var post = yield db.Post.find({where: {url: urls[i] }});

        // 检查是否存在
        if(_.isEmpty(post)) {
            console.log('==================');
            var newPost = yield getPost(urls[i]);
            yield db.Post.create(newPost);
        }
    }

    this.body = 'done';
};

