'use strict';

/*
 * 不用授权的接口 by forsigner
 */
var php = require('phpjs');
var moment = require('moment');
var _ = require('underscore');
_.str = require('underscore.string');
var forvy = require('forvy');

var db = require('../models');
var helper = require('../libs/helper');
var crawler = require('../libs/crawler');


//----------------------------------------------------------
// 任务
exports.getTasks = function * () {

    var query = this.query;
    var today = yield db.Task.max('updatedAt'); // 把数据库最新的一天左右最近一天

    // 如果是小钱活动
    if(query.taskType == '1') query.taskType = [11, 12, 13];
    if(query.taskType == '2') query.taskType = [21, 22];

    // 获取分页查询条件
    var condition = helper.getCondition({
        where: {taskType: query.taskType},
        page: query.page,
        perPage: query.perPage
    });


    // 是否是查询今日
    if(query.isToday == 'true' || query.isToday == true) {
        condition.where.updatedAt = {between: [moment(today).format('YYYY-MM-DD'), moment().add(1, 'days').format('YYYY-MM-DD')]};
    }

    // 如果存在channel
    if(query.auditType) {
       condition.where.auditType = parseInt(query.auditType);
    }


    var data = yield db.Task.findAndCountAll(condition);
    this.set({'X-Pagination-Total-Count': data.count});
    this.body = data.rows;
};

// 单个任务
exports.getTask = function * () {

    // var urlTaskDetail = 'http://api.ibk.com/page/task-detail?taskId=' + this.params.taskId;
    // var urlTaskCompare = 'http://api.ibk.com/page/task-compare?taskId=' + this.params.taskId;
    var urlTaskDetail = 'http://api.ibaokuan.net/page/task-detail?taskId=' + this.params.taskId;
    var urlTaskCompare = 'http://api.ibaokuan.net/page/task-compare?taskId=' + this.params.taskId;

    var fragmentDetail = yield crawler.getHtmlFragment(urlTaskDetail);
    var fragmentCompare = yield crawler.getHtmlFragment(urlTaskCompare);

    var task = yield db.Task.find({where: {id: this.params.taskId}});

    var fragment = {
        fragmentDetail: fragmentDetail,
        fragmentCompare: fragmentCompare,
    };

    this.body = _.extend(task.dataValues, fragment);
};

// 获取红包订单列表
exports.getTaskOrders = function * () {

    var taskId = this.params.taskId;
    var query = this.query;

    // 获取分页查询条件
    var condition = helper.getCondition({
        include: [db.Buyer],
        where: {taskId: taskId},
        page: query.page,
        perPage: query.perPage
    });

    if(!_.isEmpty(query.status)) {
        condition.where.status = query.status;
    }

    // 查询数据
    var data = yield db.TaskOrder.findAndCountAll(condition);

    this.set({'X-Pagination-Total-Count': data.count});

    var taskOrders = []; // 红包订单

    _.each(data.rows, function(value, key) {
        taskOrders.push({
            buyerId: value.Buyer.id,
            nickname: value.Buyer.nickname,
            avatar: value.Buyer.avatar
        });
    });

    this.body = taskOrders;
};

//----------------------------------------------------------

// 达人榜单
exports.getTopLists = function * () {
    this.body = yield db.Buyer.findAll({order: 'totalIncome DESC', limit: 30});
};


// 获取淘宝信息
exports.getTaobaoAccountInfo = function * () {

    // 获取淘宝账号信息
    this.body = yield crawler.getTaobaoAccountInfo(this.query.name);
};

