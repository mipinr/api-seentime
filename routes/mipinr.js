'use strict';

/*
 * 不用授权的接口 by forsigner
 */
var php = require('phpjs');
var moment = require('moment');
var _ = require('underscore');
_.str = require('underscore.string');
var request = require('request');
var cheerio = require('cheerio');
var gbk = require('gbk');
var crypto = require('crypto');
var url = require('url');
var querystring = require('querystring');

var db = require('../models');
var oauthConfig = require('../libs/oauthConfig');
var helper = require('../libs/helper');
var crawler = require('../libs/crawler');

var views = require('co-views');
var parse = require('co-body');
var co = require('co');
var iconv = require('iconv-lite');
var BufferHelper = require('bufferhelper');

// 获取品牌
exports.getBrands = function * () {

    var query = this.query;

    if(query.buyerId) {
        // 获取分页查询条件
        var condition = helper.getCondition({
            include: [
                {
                    model: db.Buyer,
                    where: {
                        id: query.buyerId
                    }
                }
            ],
            page: query.page,
            perPage: query.perPage
        });

        var data = yield db.Brand.findAndCountAll(condition);
        this.set({'X-Pagination-Total-Count': data.count});
        this.body = data.rows;
    } else {
        // 获取分页查询条件
        var condition = helper.getCondition({
            include: {
                model: db.Item,
                attributes: ['picUrl'],
            },
            attributes: ['id','name','story', 'logoUrl', 'coverUrl', 'followerCount', 'itemCount', 'createdAt', 'updatedAt'],
            page: query.page,
            perPage: query.perPage
        });

        if(query.popular == 'true' || query.popular == true || query.popular == 1) condition.limit = 10;
        if(query.firstLetter) condition.where.firstLetter = query.firstLetter;
        if(query.categoryId) condition.where.categoryId = query.categoryId;

        var data = yield db.Brand.findAndCountAll(condition);
        this.set({'X-Pagination-Total-Count': data.count});
        this.body = data.rows;

    }


};


exports.getBrand = function * () {
    this.body = yield db.Brand.find({where: {id: this.params.brandId}});
};

exports.getBrandBuyers = function * () {

    var query = this.query;

    // 获取分页查询条件
    var condition = helper.getCondition({
        include: {
            model: db.Brand,
            where: {
                // id: this.pramas.brandId
                id: 1
            }
        },
        page: query.page,
        perPage: query.perPage
    });


    var data = yield db.Buyer.findAndCountAll(condition);
    this.set({'X-Pagination-Total-Count': data.count});
    this.body = data.rows;
};



// 获取Topic
exports.getTopics = function * () {

    var query = this.query;

    if(query.buyerId) {
        // 获取分页查询条件
        var condition = helper.getCondition({
            include: [
                {
                    model: db.Buyer,
                    where: {
                        id: query.buyerId
                    }
                }
            ],
            page: query.page,
            perPage: query.perPage
        });

        var data = yield db.Topic.findAndCountAll(condition);
        this.set({'X-Pagination-Total-Count': data.count});
        this.body = data.rows;
    } else {

        // 获取分页查询条件
        var condition = helper.getCondition({
            page: query.page,
            perPage: query.perPage
        });

        // 是否popular
        if(query.popular == 'true' || query.popular == true || query.popular == 1) condition.limit = 8;
        if(query.firstLetter) condition.where.firstLetter = query.firstLetter;
        if(query.categoryId) condition.where.categoryId = query.categoryId;

        var data = yield db.Topic.findAndCountAll(condition);
        this.set({'X-Pagination-Total-Count': data.count});
        this.body = data.rows;

    }


};


exports.getTopic = function * () {
    this.body = yield db.Topic.find({where: {id: this.params.topicId}});
};

exports.getTopicBuyers = function * () {

    var query = this.query;

    // 获取分页查询条件
    var condition = helper.getCondition({
        include: {
            model: db.Topic,
            where: {
                // id: this.pramas.brandId
                id: 1
            }
        },
        page: query.page,
        perPage: query.perPage
    });

    var data = yield db.Buyer.findAndCountAll(condition);
    this.set({'X-Pagination-Total-Count': data.count});
    this.body = data.rows;
};



// 获取Seckill
exports.getSeckills = function * () {

    var query = this.query;

    // 获取分页查询条件
    var condition = helper.getCondition({
        page: query.page,
        perPage: query.perPage
    });

    var data = yield db.Seckill.findAndCountAll(condition);
    this.set({'X-Pagination-Total-Count': data.count});
    this.body = data.rows;

};


exports.getSeckill = function * () {
    this.body = yield db.Seckill.find({where: {id: this.params.seckillId}});
};


// login with qq
exports.loginByQq = function * () {

    var query = this.query;

    var queryString = {
        grant_type: oauthConfig.qq.grant_type,
        client_id: oauthConfig.qq.client_id,
        client_secret: oauthConfig.qq.client_secret,
        redirect_uri: oauthConfig.qq.redirect_uri,
        state: 2,
        code: query.code
    };

    // 获取token
    var token = yield new Promise(function(resolve, reject) {

        request({
            method: 'GET',
            uri: oauthConfig.qq.token_url,
            encoding : null, // 中文乱码问题
            qs: queryString // 查询字符串
            }, function (error, response, body) {
                if (body.indexOf('error_description') < -1) {

                    // 结局中文乱码问题
                    var buf = iconv.decode(body, 'gb2312');
                    resolve(buf);
                } else {
                    var buf = iconv.decode(body, 'gb2312');
                    // resolve(JSON.parse(buf);
                    console.log(buf);
                    resolve(buf);
                }
        });

    });

    var objToken = querystring.parse(url.parse('http://www.something.com?' + token).query)

    // 获取opendid
    var openidString = yield new Promise(function(resolve, reject) {

        request({
            method: 'GET',
            uri: oauthConfig.qq.openid_url,
            encoding : null, // 中文乱码问题
            qs: {access_token: objToken.access_token} // 查询字符串
            }, function (error, response, body) {
                if (body.indexOf('error_description') < -1) {

                    // 结局中文乱码问题
                    var buf = iconv.decode(body, 'gb2312');
                    resolve(buf);
                } else {
                    var buf = iconv.decode(body, 'gb2312');
                    // resolve(JSON.parse(buf);
                    console.log(buf);
                    resolve(buf);
                }
        });

    });


    //callback( {"client_id":"101202182","openid":"ED5661D48D300A359908BECD6563AA3C"} )
    var jsonString = openidString.substring(openidString.indexOf('{') - 1, openidString.indexOf('}') + 1);
    var openid = JSON.parse(jsonString).openid;

    var data = {

    };


    // 用户信息
    var user = yield new Promise(function(resolve, reject) {

        request({
            method: 'GET',
            uri: 'https://graph.qq.com/user/get_user_info',
            qs: {

                access_token: objToken.access_token,
                oauth_consumer_key: oauthConfig.qq.client_id,
                openid: openid
            }
        },function (error, response, body) {
            resolve(body);
        });
    });

    // var token = yield promise;
    this.body = user;


    // this.body = data;

    // // 检查用户
    // var buyer = yield db.Buyer.find({where: {openid: token.uid }});

    // // 手机是否被注册
    // if(!_.isEmpty(buyer)) {
    //     this.body = {code: 422, message: '此手机已被注册'};
    // } else {

    //     // 设置默认秘密
    //     var password = 'mipinr_qwery';

    //     // 密码两次加密
    //     password = crypto.createHash('sha1').update(password).digest('hex');
    //     password = crypto.createHash('sha1').update(password).digest('hex');

    //     var username = 'jd' + token.uid;

    //     var newBuyer = {
    //         username: username,
    //         password: password,
    //         avatar: 'http://gd1.alicdn.com/imgextra/i1/2207718142/TB29nOzbXXXXXbHXXXXXXXXXXXX_!!2207718142.jpg_160x160.jpg',
    //         nickname: token.user_nick
    //     };

    //     // 保存用户
    //     // yield db.Buyer.create(body);
    //     // this.body = yield db.Buyer.find({where: {openid: token.openid }});
    //     this.body = newBuyer;
    // }



};




// login with jd
exports.loginByJd = function * () {

    var query = this.query;

    var formData = {
        grant_type: oauthConfig.jd.grant_type,
        client_id: oauthConfig.jd.client_id,
        client_secret: oauthConfig.jd.client_secret,
        redirect_uri: oauthConfig.jd.redirect_uri,
        state: 2,
        code: query.code
    };

    // 返回promise
    var promise = new Promise(function(resolve, reject) {

        request({
            method: 'POST',
            uri: oauthConfig.jd.token_url,
            // encoding: 'utf8',
            encoding : null, // 结局中文乱码问题
            form: formData
            }, function (error, response, body) {
                if (!error) {

                    // 结局中文乱码问题
                    var buf = iconv.decode(body, 'gb2312');
                    resolve(buf);
                } else {
                    reject(error);
                }
        });

    });


    var token = yield promise;
    this.body = token.code;

    // // 检查用户
    // var buyer = yield db.Buyer.find({where: {openid: token.uid }});

    // // 手机是否被注册
    // if(!_.isEmpty(buyer)) {
    //     this.body = {code: 422, message: '此手机已被注册'};
    // } else {

    //     // 设置默认秘密
    //     var password = 'mipinr_qwery';

    //     // 密码两次加密
    //     password = crypto.createHash('sha1').update(password).digest('hex');
    //     password = crypto.createHash('sha1').update(password).digest('hex');

    //     var username = 'jd' + token.uid;

    //     var newBuyer = {
    //         username: username,
    //         password: password,
    //         avatar: 'http://gd1.alicdn.com/imgextra/i1/2207718142/TB29nOzbXXXXXbHXXXXXXXXXXXX_!!2207718142.jpg_160x160.jpg',
    //         nickname: token.user_nick
    //     };

    //     // 保存用户
    //     // yield db.Buyer.create(body);
    //     // this.body = yield db.Buyer.find({where: {openid: token.openid }});
    //     this.body = newBuyer;
    // }



};



// login with jd
exports.test = function * () {
    // var str = '毛衣  衬衫  背心  卫衣  开衫  牛仔外套  外套  针织衫  西装  马夹  棒球服  白衬衫  T恤';
    // var str = '连衣裙  半身裙  长裙  短裙  背心裙  蓬蓬裙  背带裙  小黑裙  包臀裙';
    // var str = '短裤  裙裤  休闲裤  牛仔裤  打底裤  铅笔裤  小脚裤  背带裤';
    // var str = '清洁  补水保湿  美白防晒  抗敏舒缓  控油祛痘  身体润肤  眼部问题';
    // var str = '马克杯  玻璃杯  保温杯  咖啡杯  随手杯  情侣杯  星巴克';
    // var str = '收纳盒  收纳袋  收纳箱  收纳篮  化妆品收纳  收纳柜  衣架  收纳架';
    // var str = '热水袋  伞  口罩  纸巾盒  相框/相册  洗护用具  垃圾桶  宠物  台历';
    // var str = '碗  厨房用具  盘子  烘培  饭盒  锅  酒具  茶具  筷子  咖啡机';
    // var str = '笔记本  明信片  笔 钢笔  胶带  印章  笔袋  日记本  Lamy  手账相关  软毛笔';
    // var str = '宝宝用品  童装  孕妇  儿童玩具  宝宝营养';
    var str = '毛绒玩具  手办  小黄人  银魂  模型  EVA  拼图  海贼王  动漫  娃娃  高达';
/*
杯子
马克杯  玻璃杯  保温杯  咖啡杯  随手杯  情侣杯  星巴克

收纳整理
收纳盒  收纳袋  收纳箱  收纳篮  化妆品收纳  收纳柜  衣架  收纳架

生活日用
热水袋  伞  口罩  纸巾盒  相框/相册  洗护用具  垃圾桶  宠物  台历

厨房餐饮
碗  厨房用具  盘子  烘培  饭盒  锅  酒具  茶具  筷子  咖啡机

文具
笔记本  明信片  笔 钢笔  胶带  印章  笔袋  日记本  Lamy  手账相关  软毛笔

母婴
宝宝用品  童装  孕妇  儿童玩具  宝宝营养

玩具
毛绒玩具  手办  小黄人  银魂  模型  EVA  拼图  海贼王  动漫  娃娃  高达
*/


    // var data = str.split('  ');
    // _.each(data, function(value) {
    //     db.Category.update({appType: 2
    //     },{where: {appType:3}} );

        // db.Category.create({
        //     name: value,
        //     appType: 3,
        //     primaryCategoryId: 106,
        //     primaryCategoryName: '玩具',
        //     secondaryCategoryId: 60006,
        //     secondaryCategoryName: '百货'
        // });
    // });

        db.Category.update({
            englishName: 'Commodity'
        },{
            where: {
                primaryCategoryId: 106
            }
        } );

    this.body = 'done';
};


