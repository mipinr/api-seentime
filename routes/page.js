'use strict';

var co = require('co');
var swig  = require('swig');
var views = require('co-views');

var db = require('../models');

var render = views(__dirname + '/../views', {
  map: {
    html: 'swig'
  }
});

// task详情
exports.taskDetail = function * () {

    var taskId = this.query.taskId;
    var task = yield db.Task.find({where: {id: this.query.taskId}});

    this.body = yield render('task-detail', {
        task: task
    });
};

// task对照
exports.taskCompare = function * () {

    var taskId = this.query.taskId;
    var task = yield db.Task.find({where: {id: this.query.taskId}});

    this.body = yield render('task-compare', {
        task: task
    });
};

