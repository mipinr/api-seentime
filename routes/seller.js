/*
 * 商家接口 by forsigner
 */

var db = require('../models')
var helper = require('../libs/helper');
var crawler = require('../libs/crawler');
var _ = require('underscore');
_.str = require('underscore.string');

// 获取用户信息
exports.getSellerInfo = function * (next) {
    var accessToken = helper.getToken(this);
    var seller = yield db.OauthAccessToken.find({where: {accessToken: accessToken}});
    this.body = yield db.Seller.find({ where: {id: seller.sellerId}});
};

exports.getSeller = function * () {

    this.body = 'getSeller';
};

exports.updateSeller = function * () {
    this.body = 'updateSeller';
};

// 获取商品列表
exports.getItems = function * () {

   var query = this.query;

    // 获取分页查询条件
    var condition = helper.getCondition({
        where: {sellerId: this.params.sellerId },
        page: query.page,
        perPage: query.perPage
    });

    if(query.channel) {
        condition.where.channel = query.channel;
    }

    var data = yield db.Item.findAndCountAll(condition);
    this.set({'X-Pagination-Total-Count': data.count});
    this.body = data.rows;
};

// 创建商品
exports.createItem = function * () {

    var body = this.request.body || {};
    body.sellerId = this.params.sellerId;

    // 创建商品
    var newItem = yield db.Item.create(body);
    newItem.id = yield db.Item.max('id');
    this.body = newItem;
};


// 修改商品
exports.updateItem = function * () {
    var body = this.request.body || {};
    yield db.Item.update(body, {where:{id: this.params.itemId}});
    this.body = yield db.Item.find({where:{id: this.params.itemId}});
};


// 获取品牌列表
exports.getBrands = function * () {

   var query = this.query;

    // 获取分页查询条件
    var condition = helper.getCondition({
        where: {sellerId: this.params.sellerId },
        page: query.page,
        perPage: query.perPage
    });

    var data = yield db.Brand.findAndCountAll(condition);
    this.set({'X-Pagination-Total-Count': data.count});
    this.body = data.rows;
};

// 创建品牌
exports.createBrand = function * () {
};


// 修改品牌
exports.updateBrand = function * () {
};


// 获取专题列表
exports.getTopics = function * () {

   var query = this.query;

    // 获取分页查询条件
    var condition = helper.getCondition({
        where: {sellerId: this.params.sellerId },
        page: query.page,
        perPage: query.perPage
    });

    var data = yield db.Topic.findAndCountAll(condition);
    this.set({'X-Pagination-Total-Count': data.count});
    this.body = data.rows;
};

// 创建专题
exports.createTopic = function * () {
};


// 修改专题
exports.updateTopic = function * () {
};

// 获取秒杀列表
exports.getSeckills = function * () {
   var query = this.query;

    // 获取分页查询条件
    var condition = helper.getCondition({
        where: {sellerId: this.params.sellerId },
        page: query.page,
        perPage: query.perPage
    });

    var data = yield db.Seckill.findAndCountAll(condition);
    this.set({'X-Pagination-Total-Count': data.count});
    this.body = data.rows;
};

// 创建秒杀
exports.createSeckill = function * () {
};


// 修改秒杀
exports.updateSeckill = function * () {
};

//============================================================
// 任务
// 获取任务列表
exports.getTasks = function * () {

   var query = this.query;

    if(query.taskType == '1') query.taskType = [11, 12, 13];
    if(query.taskType == '2') query.taskType = [21, 22];

    // 获取分页查询条件
    var condition = helper.getCondition({
        where: {
            sellerId: this.params.sellerId,
            taskType: query.taskType
        },
        page: query.page,
        perPage: query.perPage
    });

    var data = yield db.Task.findAndCountAll(condition);
    this.set({'X-Pagination-Total-Count': data.count});
    this.body = data.rows;
};

// 创建task
exports.createTask = function * () {

    var body = this.request.body || {};
    body.sellerId = this.params.sellerId;

    // 创建任务
    var newTask = yield db.Task.create(body);
    newTask.id = yield db.Task.max('id');
    this.body = newTask;
    // this.body = body;

};

// 修改task
exports.updateTask = function * () {
    var body = this.request.body || {};
    yield db.Task.update(body, {where:{id: this.params.taskId}});
    this.body = yield db.Task.find({where:{id: this.params.taskId}});
};

// 获取task订单列表
exports.getTaskOrders = function * () {

    var query = this.query;

    if(query.taskType == '1') query.taskType = [11, 12, 13];
    if(query.taskType == '2') query.taskType = [21, 22];

    // 获取分页查询条件
    var condition = helper.getCondition({
        include: [
            {model: db.Task, where: {title: {like: ''}, taskType: query.taskType}},
            {model: db.Buyer, where: {nickname: {like: ''}}},
            {model: db.Taobao, where: {name: {like: ''}}},
            {model: db.Seller}
        ],
        where: {
            sellerId: this.params.sellerId,
            status: '',
            orderNumber: {like: ''}
        },
        page: query.page,
        perPage: query.perPage
    });

    // 获取对应status的红包
    if(query.status) {
        condition.where.status = query.status;
    } else {
        delete condition.where.status;
    }

    // 搜索商品名
    if(query.taskTitle) {
        condition.include[0].where.title.like = '%' + query.taskTitle + '%';
    } else {
        delete condition.include[0].where.title;
    }

    // 搜索用户昵称
    if(query.buyerNickname) {
        condition.include[1].where.nickname.like = '%' + query.buyerNickname + '%';
    } else {
        delete condition.include[1].where;
    }

    // 搜索淘宝账号
    if(query.taobaoName) {
        condition.include[2].where.name.like = '%' + query.taobaoName + '%';
    } else {
        delete condition.include[2].where;
    }

    // 搜索订单号
    if(query.orderNumber) {
        condition.where.orderNumber.like = '%' + query.orderNumber + '%';
    } else {
        delete condition.where.orderNumber;
    }

    console.log(condition);
    var data = yield db.TaskOrder.findAndCountAll(condition);
    this.set({'X-Pagination-Total-Count': data.count});
    this.body = data.rows;
};


// task订单审核操作
exports.updateTaskOrder = function * () {

    var body = this.request.body || {};

    yield db.TaskOrder.update(body, {where:{id: this.params.taskOrderId}});
    var newTaskOrder = yield db.TaskOrder.find({
        include: [db.Task],
        where:{id: this.params.taskOrderId}
    });

    // 创建消息
    var message = {
        buyerId: newTaskOrder.buyerId,
        taskOrderId: newTaskOrder.id,
        messateItemId: newTaskOrder.id,
        title: newTaskOrder.Task.title,
        picUrl: newTaskOrder.Task.picUrl
    };

    switch (body.status) {

        // 资格审核不通过
        case '11':
            message.type = 11;
            message.content = '您申请的该商品的试用审核不通过';
            var newMessege = yield db.Message.create(message);
            break;

        // 资格审核通过
        case '20':
            message.type = 20;
            message.content = '您申请的该商品的试用审核通过';
            var newMessege = yield db.Message.create(message);

            break;

        // 订单审核通过
        case '22':
            message.type = 22;
            message.content = '该商品的订单号审核通过';
            var newMessege = yield db.Message.create(message);
            break;

        // 订单审核不通过
        case '23':
            message.type = 23;
            message.content = '该商品的订单号审核不通过';
            var newMessege = yield db.Message.create(message);
            break;

        // 返款审核通过
        case '41':

            message.type = 23;
            message.content = '该商品的返款审核通过';
            var newMessege = yield db.Message.create(message);
            break;

        // 返款审核不通过
        case '42':
            message.type = 42;
            message.content = '该商品的返款审核不通过';
            var newMessege = yield db.Message.create(message);
            break;
        default:
            break;
    }

    this.body = newTaskOrder;
};

//============================================================

// 获取财务记录
exports.getFinanceRecords = function * () {

    var query = this.query;

    // 获取分页查询条件
    var condition = helper.getCondition({
        where: {
            sellerId: this.params.sellerId,
        },
        page: query.page,
        perPage: query.perPage
    });

    var data = yield db.SellerFinanceRecord.findAndCountAll(condition);
    this.set({'X-Pagination-Total-Count': data.count});
    this.body = data.rows;
};
