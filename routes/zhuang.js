'use strict';

/*
 * 不用授权的接口 by forsigner
 */
var php = require('phpjs');
var moment = require('moment');
var _ = require('underscore');
_.str = require('underscore.string');
var request = require('request');
var cheerio = require('cheerio');
var gbk = require('gbk');
var crypto = require('crypto');
var url = require('url');
var querystring = require('querystring');

var db = require('../models');
var helper = require('../libs/helper');


// 获取装园
exports.getCollections = function * () {

    var query = this.query;

    // 获取分页查询条件
    var condition = helper.getCondition({
        include: [db.Item],
        page: query.page,
        perPage: query.perPage
    });

    var data = yield db.Collection.findAndCountAll(condition);
    this.set({'X-Pagination-Total-Count': data.count});
    this.body = data.rows;
};


exports.getCollection = function * () {

    var query = this.query;

    // 获取分页查询条件
    var condition = helper.getCondition({
        include: [db.Item],
        where: {
            id: this.params.collectionId
        },
        page: query.page,
        perPage: query.perPage
    });

    var data = yield db.Collection.find(condition);
    this.set({'X-Pagination-Total-Count': data.Items.length});
    this.body = data;
};
