'use strict';

/*
 * 通用接口
 */

var bodyParser = require('koa-body-parser');

//==================路由=========================
var admin = require('../routes/admin');

var helper = require('../libs/helper.js');

exports.getRouters = function(route) {

  route
  .get('/:adminId', admin.getAdmin)
  .patch('/:adminId', admin.updateAdmin)
  .get('/:adminId/items', admin.getItems)
  .post('/:adminId/items',
      helper.checkParams({ require: ['itemUrl', 'type'], allow: ['itemUrl', 'type'] }),
      admin.createItem
  );

};
