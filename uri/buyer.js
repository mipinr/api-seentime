'use strict';

/*
 * 通用接口
 */

var bodyParser = require('koa-body-parser');

//==================路由=========================
var mipinr = require('../routes/mipinr'),
    buyer = require('../routes/buyer'),
    apiGuide = require('../routes/api_guide');

var helper = require('../libs/helper.js');

exports.getRouters = function(route) {

  route

  // 验证url的buyerId和accessToken的buyerId是否一致
  // todo 这个有点问题
  /*
  .param('buyerId', function *(buyerId, next) {

      // 获取accessToken
      var accessToken = helper.getToken(this);
      var data = yield db.OauthAccessToken.find({where: {accessToken: accessToken}});

      if(data.buyerId == buyerId) {
          yield next;
      } else {
          this.body = {code: 400, message: 'token错误'};
      }
  })
  */
  .get('/info', buyer.getBuyerInfo)
  .get('/:buyerId', buyer.getBuyer)
  .patch('/:buyerId',
      bodyParser(),
      helper.checkParams({ allow: ['username', 'nickname', 'realname', 'phone', 'email', 'gender', 'avatar', 'qq', 'zhifubao', 'birthday'] }),
      buyer.updateBuyer
  )

  .get('/:buyerId/items', buyer.getItems)
  .post('/:buyerId/items',
      bodyParser(),

      // helper.checkParams({require: ['itemUrl', 'picUrl', 'title']}),
      buyer.createItem
  )
  .patch('/:buyerId/items/:itemId',
      bodyParser(),

      // helper.checkParams({allow: ['status', 'orderNumber']}),
      buyer.updateItem
  )

  .get('/:buyerId/task_orders',

      // helper.checkParams({allow: ['status', 'type', 'page', 'perPage']}),
      buyer.getTaskOrders
  )
  .post('/:buyerId/task_orders',
      bodyParser(),

      // helper.checkParams({require: ['sellerId','hongbaoId', 'taobaoId'] , allow: ['sellerId', 'hongbaoId', 'taobaoId']}),
      buyer.createTaskOrder
  )
  .patch('/:buyerId/task_orders/:taskOrderId',
      bodyParser(),

      // helper.checkParams({allow: ['status', 'orderNumber']}),
      buyer.updateTaskOrder
  )

  .get('/:buyerId/taobaos', buyer.getTaobaos)
  .post('/:buyerId/taobaos',
      bodyParser(),
      helper.checkParams({ require: ['name'], allow: ['name'] }),
      buyer.createTaobao
  )
  .delete('/:buyerId/taobaos/:taobaoId', bodyParser(), buyer.deleteTaobao)

  .get('/:buyerId/exchange',

      // helper.checkParams({require: ['name'] , allow: ['name']}),
      buyer.exchange
  )

  .get('/:buyerId/messages', buyer.getMessages)
  .get('/:buyerId/messages/:messageItemId/details', buyer.getMessageDetails)

  // 获取装园
  .get('/:buyerId/collections',

      // helper.checkParams({allow: ['status', 'type', 'page', 'perPage']}),
      buyer.getCollections
  );

};
