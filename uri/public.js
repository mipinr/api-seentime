'use strict';

/*
 * 通用接口(不用授权)
 */

var bodyParser = require('koa-body-parser');
var helper = require('../libs/helper.js');

//==================路由=========================
var common = require('../routes/common'),
    ibaokuan = require('../routes/ibaokuan'),
    zhuang = require('../routes/zhuang'),
    mipinr = require('../routes/mipinr'),
    forsigner = require('../routes/forsigner'),
    page = require('../routes/page'),
    duiba = require('../routes/duiba'),
    apiGuide = require('../routes/api_guide');

exports.getRouters = function(route, oauthBuyer, oauthSeller) {

  route

  //===================登录接口=======================
  // 商家登录
    .post('/seller/authorize', function *(next)  {
      this.set('Content-Type', 'application/json');
      yield next;
    }, oauthSeller.grant())

    // 用户登录
    .post('/buyer/authorize', function *(next)  {
      this.set('content-type', 'application/json');
      yield next;
    }, oauthBuyer.grant())

    //===================guide=======================
    .get('/', apiGuide.index)

    //===================静态html页面=======================
    .get('/page/task_detail', page.taskDetail)
    .get('/page/task_compare', page.taskCompare)

    //===================兑吧=======================
    .get('/duiba/credit_query', duiba.creditQuery) // 余额查询
    .get('/duiba/credit_consume', duiba.creditConsume) // 兑换
    .get('/duiba/order_status', duiba.orderStatus) //兑吧订单查询接口
    .get('/duiba/credit_notify', duiba.creditNotify) //兑吧订单查询接口
    .get('/duiba/order_status', duiba.orderStatus) //订单状态

    //===================通用接口=======================

    .get('/jds', common.jds)
    .get('/jd', common.jd)

    .get('/tag', common.tag)

    // 配置
    .get('/config', common.getConfig)

    // banner
    .get('/banners', common.getBanners)

    .get('/entrys', common.getEntrys)

    .get('/users', common.getUsers)
    .get('/users/:userId', common.getUser)

    .get('/items',
        helper.checkParams({ allow: ['channel', 'source', 'categoryId', 'industryId', 'collectionId', 'brandId', 'buyerId', 'topicId', 'primaryCategoryId', 'secondaryCategoryId', 'seckillId', 'isPopular', 'isToday', 'page', 'perPage'] }),
        common.getItems
    )
    .post('/items',
        bodyParser(),
        common.createItem
    )
    .get('/items/:itemId', common.getItem)

    .get('/categorys',
        helper.checkParams({ allow: ['type'] }),
        common.getCategorys
    )

    .get('/item_tags', common.getItemTags)

    .post('/item_info',
        bodyParser(),
        common.getItemInfo
    )

    .post('/item_history_price',
        bodyParser(),
        common.getItemHistoryPrice
    )

    .post('/buyer/register',
        bodyParser(),
        helper.checkRegisterParams,
        common.buyerRegister
    )
    .patch('/buyer/reset_password',
        bodyParser(),
        helper.checkParams({ require: ['password', 'phone'], allow: ['password', 'phone'] }),
        common.buyerResetPassword
    )
    .post('/seller/register', bodyParser(), helper.checkRegisterParams, common.sellerRegister)

    .get('/search', helper.checkParams({ require: ['type'], allow: ['q', 'type', 'page', 'perPage'] }), common.search)

    //===================爱爆款接口=======================
    // 任务，红包、试用、小钱都是一种任务
    .get('/tasks',
        helper.checkParams({ require: ['taskType'], allow: ['taskType', 'status', 'page', 'perPage'] }),
        ibaokuan.getTasks
    )
    .get('/tasks/:taskId', ibaokuan.getTask)
    .get('/tasks/:taskId/task_orders',
        helper.checkParams({ allow: ['taskType', 'status', 'page', 'perPage'] }),
        ibaokuan.getTaskOrders
    )

    .get('/top_lists', ibaokuan.getTopLists)
    .get('/taobao_account_info',
        helper.checkParams({ require: ['name'], allow: ['name'] }),
        ibaokuan.getTaobaoAccountInfo
    )

    //===================装接口=======================

    .get('/collections', zhuang.getCollections)
    .get('/collections/:collectionId', zhuang.getCollection)

    //===================觅品儿=======================
    .get('/brands',
        helper.checkParams({ allow: ['categoryId', 'firstLetter', 'isPopular', 'buyerId', 'page', 'perPage'] }),
        mipinr.getBrands
    )
    .get('/brands/:brandId', mipinr.getBrand)
    .get('/brands/:brandId/buyers', mipinr.getBrandBuyers)

    .get('/topics',
        helper.checkParams({ allow: ['categoryId', 'isPopular', 'buyerId', 'page', 'perPage'] }),
        mipinr.getTopics
    )
    .get('/topics/:topicId', mipinr.getTopic)
    .get('/topics/:topicId/buyers', mipinr.getTopicBuyers)
    .get('/seckills', mipinr.getSeckills)
    .get('/seckills/:seckillId', mipinr.getSeckill)

    .get('/login_by_qq',
        helper.checkParams({ require: ['code'], allow: ['code'] }),
        mipinr.loginByQq
    )

    .get('/login_by_jd',
        helper.checkParams({ require: ['code'], allow: ['code'] }),
        mipinr.loginByJd
    )

    .get('/test',
        mipinr.test
    )

    //===================forsigner blog=======================
    .get('/posts', forsigner.getPosts)
    .get('/create_posts', forsigner.createPosts);
};
