'use strict';

/*
 * 通用接口
 */

var bodyParser = require('koa-body-parser');

//==================路由=========================
var mipinr = require('../routes/mipinr'),
    seller = require('../routes/seller'),
    apiGuide = require('../routes/api_guide');

var helper = require('../libs/helper.js');

exports.getRouters = function(route) {

  route

  // 验证url的buyerId和accessToken的buyerId是否一致
  // todo 这个有点问题
  /*
  .param('sellerId', function *(sellerId, next) {

      // 获取accessToken
      var accessToken = helper.getToken(this);
      var data = yield db.OauthAccessToken.find({where: {accessToken: accessToken}});

      if(data.sellerId == sellerId) {
          yield next;
      } else {
          this.body = {code: 400, message: 'token错误'};
      }
  })
  */

  .get('/info', seller.getSellerInfo)
  .get('/:sellerId', seller.getSeller)
  .patch('/:sellerId', seller.updateSeller)

  .get('/:sellerId/items', seller.getItems)
  .post('/:sellerId/items',
      bodyParser(),

      // helper.checkParams({require: ['itemUrl', 'picUrl', 'title']}),
      seller.createItem
  )
  .patch('/:sellerId/items/:itemID',
      bodyParser(),

      // helper.checkParams({allow: ['status', 'orderNumber']}),
      seller.updateItem
  )

  .get('/:sellerId/brands', seller.getBrands)
  .post('/:sellerId/brands',
      bodyParser(),

      // helper.checkParams({require: ['itemUrl', 'picUrl', 'title']}),
      seller.createBrand
  )
  .patch('/:sellerId/brands/:brandID',
      bodyParser(),

      // helper.checkParams({allow: ['status', 'orderNumber']}),
      seller.updateBrand
  )

  .get('/:sellerId/topics', seller.getTopics)
  .post('/:sellerId/topics',
      bodyParser(),

      // helper.checkParams({require: ['itemUrl', 'picUrl', 'title']}),
      seller.createTopic
  )
  .patch('/:sellerId/topics/:topicID',
      bodyParser(),

      // helper.checkParams({allow: ['status', 'orderNumber']}),
      seller.updateTopic
  )

  .get('/:sellerId/seckills', seller.getSeckills)
  .post('/:sellerId/seckills',
      bodyParser(),

      // helper.checkParams({require: ['itemUrl', 'picUrl', 'title']}),
      seller.createSeckill
  )
  .patch('/:sellerId/seckills/:seckillID',
      bodyParser(),

      // helper.checkParams({allow: ['status', 'orderNumber']}),
      seller.updateSeckill
  )

  //任务
  .get('/:sellerId/tasks', seller.getTasks)
  .post('/:sellerId/tasks',
      bodyParser(),

      // helper.checkParams({require: ['itemUrl', 'picUrl', 'title']}),
      seller.createTask
  )
  .patch('/:sellerId/tasks/:taskId',
      bodyParser(),

      // helper.checkParams({allow: ['status', 'orderNumber']}),
      seller.updateTask
  )
  .get('/:sellerId/task_orders', seller.getTaskOrders)
  .patch('/:sellerId/task_orders/:taskOrderId',
      bodyParser(),
      helper.checkParams({ require: ['status'], allow: ['status'] }),
      seller.updateTaskOrder
  )

  .get('/:sellerId/finance_records', seller.getFinanceRecords);
};
